## Interface: 20300

## Title: Tour Guide
## Notes: Powerleveling guide framework
## Author: Tekkub Stoutwrithe
## Version: Alpha
## X-Category: Quest

## SavedVariables: TourGuideAlphaDB

## OptionalDeps: Dongle, Optionhouse, TomTom, tekDebug

## DefaultState: disabled

## LoadManagers: AddonLoader
## X-LoadOn-Always: delayed

Dongle.lua
OptionHouse.lua
WidgetWarlock.lua

Locale.lua
TourGuide.lua
Parser.lua
Mapping.lua
StatusFrame.lua
OHFrame.lua
OHGuides.lua
UnlistedQuest.lua
QuestTracking.lua

TourGuide_Alliance\Guides.xml
TourGuide_Horde\Guides.xml
