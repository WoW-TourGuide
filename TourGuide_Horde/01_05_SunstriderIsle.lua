TourGuide:RegisterGuide("Sunstrider Isle (1-5)", "Eversong Woods (5-13)", "Horde", function()

return [[
A Reclaiming Sunstrider Isle
C Reclaiming Sunstrider Isle
T Reclaiming Sunstrider Isle
A Unfortunate Measures

A Warlock Training |C|Warlock|
T Warlock Training |C|Warlock|
A Windows to the Source |C|Warlock|

t Train (Level 2)

A Well Watcher Solanian
T Well Watcher Solanian
A The Shrine of Dath'Remar
A Solanian's Belongings |N|Don't forget to equip the bag he gives you|
A A Fistful of Slivers
A Thirst Unending

C Unfortunate Measures
T Unfortunate Measures
A Report to Lanthan Perilon
T Report to Lanthan Perilon
A Aggression

C Aggression
C The Shrine of Dath'Remar
C Thirst Unending
C A Fistful of Slivers
C Solanian's Belongings |N|South near a crystal, Southwest near a pond, West near treants|

T Thirst Unending
T A Fistful of Slivers
t Train (Level 4)
T The Shrine of Dath'Remar
T Solanian's Belongings
T Aggression

C Windows to the Source |C|Warlock|
A Felendren the Banished
C Felendren the Banished
T Windows to the Source |C|Warlock|
T Felendren the Banished

A Aiding the Outrunners
A Tainted Arcane Sliver |L|20483| |O|
T Tainted Arcane Sliver |O|
]]
end)

