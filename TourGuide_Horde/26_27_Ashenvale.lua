TourGuide:RegisterGuide("Ashenvale (26-27)", "Stonetalon Mountains (27)", "Horde", function()
return [[
R Splintertree Post
h Splintertree Post
A Stonetalon Standstill
A Satyr Horns
A Ashenvale Outrunners |N|Started by Kuray'bin (71,68)|
T The Ashenvale Hunt

N Kill Sharptalon |N|Spawns around (74,70). Bringing him to 1/3 health than dragging him over to the guards should help.|
C Ashenvale Outrunners
T Ashenvale Outrunners

A Torek's Assault |N|Started by Torek (68,75)|
C Torek's Assault
C Stonetalon Standstill |N|Also find and kill Tideress he drops an item which starts The Befouled Element|
N Kill Ursangous |N|Spawns around (42,67), for the Ashenvale Hunt|
N Kill Shadumbra |N|Spawns around (56,54), for the Ashenvale Hunt|
C The Sacred Flame |N|Get a phial from killing a dryad at (61,52) and fill it at the moonwell at (60,72)|

H Splintertree Post
T Stonetalon Standstill
A The Befouled Element |U|16408| |O|
T The Befouled Element |O|
A Je'neu of the Earthern Ring
T Torek's Assault
A Sharptalon's Claw |U|16305| |O|
A Ursangous's Paw |U|16305| |O|
A Shadumbra's Head |U|16304| |O|
T Sharptalon's Claw |O|
T Ursangous's Paw |O|
T Shadumbra's Head |O|
A The Hunt Completed
T The Hunt Completed

F Zoram Strand
T Je'neu of the Earthen Ring
A Between a Rock and a Thistlefur
A Troll Charm

C Between a Rock and a Thistlefur
C Troll Charm |N|Cave is at back of the camp (38,30)|
T Between a Rock and a Thistlefur
T Troll Charm
G Grind to 27 |N|If you're not already|

N Stable Pet if you dont have Bite(R4) or Claw(R4) |C|Hunter|
N Tame Ghostpaw Alpha for Bite(R4) |C|Hunter|
N Tame Elder Ashenvale Bear for Claw(R4) |C|Hunter|
N Retrieve Pet |C|Hunter|

A Destroy the Legion
C Ordanus |N|Fight to top room. Just kill Ordanus, loot the head then jump out of there.|
C Satyr Horns |N|Most annoying quest ever if you can't remove curse.|
C Destroy the Legion

R Splintertree Post |N|Don't hearth|
T Satyr Horns
T Destroy the Legion
A Diabolical Plans |U|23797| |O|
T Diabolical Plans |O|
]]
end)

