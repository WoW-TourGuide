TourGuide:RegisterGuide("Burning Steppes (52-53)", "Azshara (53-54)", "Horde", function()
    return [[
F Kargath
T Vivian Lagrave
A Dreadmaul Rock
A The Rise of the Machines (Part 1)
F Burning Steppes
A Broodling Essence
A Tablet of the Seven
N Kill Dragon Whelps and Rock Elementals/Sentries when you see them.
T Dreadmaul Rock |N|On top of Dreadmaul Rock, Southeast of FP (80.0,45.4)|
A Krom'Grul
A Krom'Grul
C Krom'Grul |N|Spawns in the caves.|
C Tablet of the Seven |N|South of FP by Dwarf Statue (54,40)|
C The Rise of the Machines (Part 1)
C Broodling Essence
T Tablet of the Seven
T Broodling Essence
A Felnok Steelspring
F Kargath
T Krom'Grul
T The Rise of the Machines (Part 1)
A The Rise of the Machines (Part 2)
T The Rise of the Machines (Part 2) |N|East from Kargath on the wall (25,46)|]]
end)

