TourGuide:RegisterGuide("Thousand Needles (27-29)", "Hillsbrad Foothills (29-30)", "Horde", function()
return [[
F Thousand Needles
T The Sacred Flame (Part 2)
A The Sacred Flame (Part 3)
A A Differend Approach

C The Sacred Flame (Part 3) |N|Charge the brazier in the cave (44,37)|

A Hypercapacitor Gizmo |N|At Whitereach Post|
T Serpent Wild
A Sacred Fire

C Sacred Fire
C Wind Rider
C Homeward Bound |N|Escort starts at (16,42) if elite gets in the way, skip it|
C Steelsnap
C A Different Approach

H Sun Rock Retreat
F Thunder Bluff
T Steelsnap |N|Hunter Rise|
A Frostmaw
T Sacred Fire |N|Elder Rise|
A Arikara

F Freewind Post
h Freewind Post
T The Sacred Flame (Part 3)
T Wind Rider

C Grimtotem Spying |N|Take the path up onto the needles (31,36).  The notes are in boxes (31,32) (33,39) (39,41)|
C Arikara
C Wanted - Arnak Grimtotem |N|North, on the edge of the zone (38,27)|
A Free at Last |N|Escort Lakota Windsong (38,27)|
C Free at Last

T Arikara |N|Back at Whitereach Post|
T Homeward Bound
T A Different Approach
A A Dip in the Moonwell

C A Dip in the Moonwell |N|On the west edge of the zone near Thalanaar (9,18).  If you have a pet you will have to dismiss it before you can control the robot.|
C Hypercapacitor Gizmo |N|Find the cage at the wrecked caravan north of Whitereach Post (22,24)|
K Galak Messenger |L|12564|

A Assassination Plot |U|12564|
T Assassination Plot |N|Back at Whitereach Post|
T Hypercapacitor Gizmo
T A Dip in the Moonwell
A Testing the Tonic
A Protect Kanati Greycloud
C Protect Kanati Greycloud
T Protect Kanati Greycloud

H Freewind Post
T Free at Last
T Wanted - Arnak Grimtotem
T Grimtotem Spying
T Testing the Tonic
]]
end)
