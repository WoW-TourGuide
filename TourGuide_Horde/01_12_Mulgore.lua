TourGuide:RegisterGuide("Mulgore (1-5)", nil, "Horde", function()

return [[
A The Hunt Begins
C The Hunt Begins
T The Hunt Begins

A Etched Note |C|Hunter|
A Simple Note |C|Warrior|
A Rune-Inscribed Note |C|Shaman|
A Verdant Note |C|Druid|

A The Hunt Continues
A A Humble Task (Part 1)

T Etched Note |C|Hunter|
T Simple Note |C|Warrior|
T Rune-Inscribed Note |C|Shaman|
T Verdant Note |C|Druid|
t Train (Level 2)

T A Humble Task (Part 1) |N|Down at the well|
A A Humble Task (Part 2)
C A Humble Task (Part 2)
T A Humble Task (Part 2)
A Rites of the Earthmother (Part 1)
C The Hunt Continues
T Rites of the Earthmother (Part 1)
A Rite of Strength
T The Hunt Continues |N|Back at camp|
A The Battleboars
A Break Sharptusk!

C The Battleboars
C Rite of Strength
C Break Sharptusk!
N Find the Dirt-stained Map |L|4851| |N|In the cave to the south of Sharptusk's hut (63.21, 82.75)|
A Attack on Camp Narache |U|4851|

H Camp Narache
T The Battleboars
T Break Sharptusk!
T Rite of Strength
T Attack on Camp Narache
A Rites of the Earthmother (Part 2)
A A Task Unfinished |N|Take the road out of camp|

R Bloodhoof Village
T Rites of the Earthmother (Part 2)
A Sharing the Land
A Rite of Vision (Part 1)
T A Task Unfinished
h Bloodhoof Village
A Dangers of the Windfury
A Swoop Hunting
A Mazzranache
A Poison Water
T Rite of Vision (Part 1)
A Rite of Vision (Part 2)
A Kyle's Gone Missing! |N|North of town, by the bridge|

N Kill and loot... |N|Kill any Swoops you see, also loot acorns under trees|
C Sharing the Land |N|To the south by the cliffs|
C Poison Water

T Sharing the Land
T Poison Water
A Winterhoof Cleansing
N Cook your ass off! |N|If you plan on leveling cooking (why wouldn't you?) buy the skill, 40x Simple Flour and 40x Mild Spices (about 3.5s total).  Cook up bread before you cook any meat that dropped.|
C Kyle's Gone Missing! |N|Find the dog running around the village and feed him |U|33009|
C Winterhoof Cleansing |N|South of town at the well (53.55, 66.42)| |U|5411|
C Rite of Vision (Part 2) |N|Grab the well stones|

T Winterhoof Cleansing
A Thunderhorn Totem
A Dwarven Digging
T Rite of Vision (Part 2)
A Rite of Vision (Part 3)
U Drink up! |U|4823|
T Kyle's Gone Missing! |N|As you pass by, don't worry you can catch up.|
T Rite of Vision (Part 3) |N|Follow the vision, or just run to the cave he's leading you to (32.68, 36.06)|
A Rite of Wisdom

C Thunderhorn Totem |N|All over the plains south of Bluff|
C Dwarven Digging |N|Kill the dorfs for their tools, then find the forge in the camp and break them.| |U|4702|
C Swoop Hunting
C Mazzranache

T Thunderhorn Totem |N|Back at Bloodhoof Village|
T Dwarven Digging
T Swoop Hunting
T Mazzranache

A Thunderhorn Cleansing
C Thunderhorn Cleansing |U|5415|
T Thunderhorn Cleansing
A Wildmane Totem

A The Ravaged Caravan |N|To the east|
C The Ravaged Caravan |N|To the north|
T The Ravaged Caravan
A The Venture Co.
A Supervisor Fizsprocket

C Dangers of the Windfury |N|To the east, near the zone border.|
C Wildmane Totem

T Wildmane Totem |N|Back at Bloodhoof Village|
A Wildmane Cleansing

A The Hunter's Path |C|Hunter|
T The Hunter's Path |C|Hunter|
A Taming the Beast (Part 1) |C|Hunter|
C Taming the Beast (Part 1) |C|Hunter| |N|North of town|
T Taming the Beast (Part 1) |C|Hunter|
A Taming the Beast (Part 2) |C|Hunter|
C Taming the Beast (Part 2) |C|Hunter| |N|North of town|
T Taming the Beast (Part 2) |C|Hunter|
A Taming the Beast (Part 3) |C|Hunter|
C Taming the Beast (Part 3) |C|Hunter| |N|East of town|
T Taming the Beast (Part 3) |C|Hunter|
N Tame a Prarie Wolf Alpha |C|Hunter| |N|For Bite (Rank 2).  Keep it for your main pet unless you find The Rake.|
A Training the Beast |C|Hunter|

A The Hunter's Way

R Thunder Bluff
A Preparation for Ceremony
T Training the Beast |C|Hunter| |N|Over on Hunter Rise (to the south)|

C Wildmane Cleansing |N|North of Bluff| |U|5416|
C The Hunter's Way
A A Sacred Burial |N|East at Red Rocks Amphitheatre|
C A Sacred Burial
C Rite of Wisdom
T A Sacred Burial
C Preparation for Ceremony |N|Harpies to the north/west|

T Preparation for Ceremony |N|Back on Bluff|
T The Hunter's Way
A Sergra Darkthornz
T Rite of Wisdom
A Rites of the Earthmother (Part 3)

C Rites of the Earthmother (Part 3) |N|North/east of Bluff.  If you cannot find him, he spawns at (49,19)|
T Rites of the Earthmother (Part 3)
A The Barrens Oases |N|From the Archdruid|

H Bloodhoof Village
A The Demon Scarred Cloak |O| |U|4854
T The Demon Scarred Cloak |O|
T Dangers of the Windfury
T Wildmane Cleansing

C Supervisor Fizsprocket |N|In the cave|
C The Venture Co.
T The Venture Co.
T Supervisor Fizsprocket

R The Barrens
R Camp Taurajo |N|Stay on the road|
A Journey to the Crossroads
f Grab flight point

R The Crossroads |N|Take the road north|
T The Barrens Oases
T Journey to the Crossroads
T Sergra Darkthornz

A A Bundle of Hides
T A Bundle of Hides
A Ride to Thunder Bluff
F Thunder Bluff
T Ride to Thunder Bluff
A Tal the Wind Rider Master
T Tal the Wind Rider Master
A Return to Jahan
F The Crossroads
T Return to Jahan

t Bows |C|Hunter| |N|Run all the way to Orgrimmar and get this from the weapon trainer.  Fly back to Crossroads|
]]

end)

