TourGuide:RegisterGuide("Blasted Lands (50-51)", "UnGoro Crater (51-52)", "Horde", function()
    return [[
R Blasted Lands
A A Boar's Vitality |N|Bloodmage Drazial (50,14)|
A Snickerfang Jowls
A The Decisive Striker
A The Basilisk's Bite
A Vulture's Vigor
A A Boar's Vitality
A Snickerfang Jowls
A The Decisive Striker
A The Basilisk's Bite
A Vulture's Vigor
A Everything Counts In Large Amounts |U|10593| |N|Near the basilisks/felhounds (51,35)|
A To Serve Kum'isha |U|8244| |N|Near the basilisks/felhounds (51,35)|
N Collect [Vulture Gizzards]x14 |L|8396 14|
N Collect [Basilisk Brain]x11 |L|8394 11|
N Collect [Blasted Boar Lungs]x6 |L|8392 6|
N Collect [Scorpok Pincers]x6 |L|8393 6|
N Collect [Snickerfang Jowls]x5 |L|8391 5|
T A Boar's Vitality
T Snickerfang Jowls
T The Decisive Striker
T The Basilisk's Bite
T Vulture's Vigor
N Abandon "To Serve Kum'isha" and "Everything Counts in Large Amounts" if you have them

H The Crossroads |N|Or whatever method is fastest to get to Gadgetzan|
F Gadgetzan
T Sprinkle's Secret Ingredient
A Delivery for Marin
T March of the Silithid
A Bungle in the Jungle
T Delivery for Marin
A Noggenfogger Elixir
T Noggenfogger Elixir
T The Stone Circle |N|(52,46)|]]
end)

