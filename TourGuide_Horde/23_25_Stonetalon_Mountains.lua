TourGuide:RegisterGuide("Stonetalon Mountains (23-25)", "Southern Barrens (25)", "Horde", function()
    return [[
R Sun Rock Retreat
A Cenarius' Legacy
T Boulderslide Ravine |N|Skip "Earthen Arise"|
A Elemental War
A Harpies Threaten
h Sun Rock Retreat
A Cycle of Rebirth
C Cycle of Rebirth
C Cenarius' Legacy
C Jin'Zil's Forest Magic
T Cycle of Rebirth
A New Life
T Cenarius' Legacy
A Ordanus

T Further Instructions (Part 2)
A Gerenzo Wrenchwhistle
C Gerenzo Wrenchwhistle
C Shredding Machines
T Gerenzo Wrenchwhistle
A Arachnophobia |N|Elite Spider, quest is from the road sign|
T Jin'Zil's Forest Magic
T Shredding Machines

H Sun Rock Retreat
T Arachnophobia
C New Life
C Elemental War
C Harpies Threaten
T New Life
T Elemental War
T Harpies Threaten
A Calling in the Reserves]]
end)

