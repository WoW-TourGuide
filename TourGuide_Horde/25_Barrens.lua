TourGuide:RegisterGuide("Southern Barrens (25)", "Thousand Needles (25-26)", "Horde", function()
    return [[
F Camp Taurajo
A Enraged Thunder Lizards
h Camp Taurajo
A A New Ore Sample
C Enraged Thunder Lizards
T Enraged Thunder Lizards
A Cry of the Thunderhawk
C Cry of the Thunderhawk
T Cry of the Thunderhawk |N|Skip Mahren Skyseer|
C Revenge of Gann (Part 1)
T Revenge of Gann (Part 1)
A Revenge of Gann (Part 2)
C Revenge of Gann (Part 2)
T Revenge of Gann (Part 2)
R The Great Lift
T Calling in the Reserves
A Message to Freewind Post]]
end)

