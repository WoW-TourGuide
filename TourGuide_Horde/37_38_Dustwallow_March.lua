TourGuide:RegisterGuide("Dustwallow March (37-38)", nil, "Horde", function()
return [[
F Dustwallow Marsh
A Theramore Spies
A The Black Shield
A Twilight of the Dawn Runner
A Hungry! |N|From Mudcrush Durtfeet, south of Brackenwall (35,37)|

C Twilight of the Dawn Runner |N|The cargo is on the top of the Northpoint Tower (46,23)|

A The Lost Report |N|From a dirt pile near Jarl's hut (55,25)|
T Soothing Spices
A Jarl Needs Eyes

C Theramore Spies
C The Black Shield
C Hungry! |N|On the peninsula (58,15)|

A Stinky's Escape |N|North of the tower (45,17).  You may want to clear the area first.|
C Stinky's Escape
C Jarl Needs Eyes

T Theramore Spies |N|Back in Brackenwall Village|
A The Theramore Docks
T The Black Shield
T Twilight of the Dawn Runner
T Hungry!

A The Severed Head |N|At Jarl's cabin (55,25)|
T Jarl Needs Eyes

C The Theramore Docks |N|Under the eastern pier (71,51).  Watch out for the shark!|

T The Theramore Docks |N|Back in Brackenwall Village|
T The Severed Head
A The Troll Witchdoctor

F Ratchet
T Stinky's Escape
]]
end)

