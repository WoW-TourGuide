TourGuide:RegisterGuide("Hillsbrad Foothills (29-30)", "Arathi Highlands (30)", "Horde", function()
return [[
F Orgrimmar
h Orgrimmar

R Hillsbrad Foothills |N|Take the zeppelin outside Orgrimmar to Undercity.  Run down thru Silverpine.|
A Time to Strike |N|You get this at Southpoint Tower (20,47) as soon as you enter Hillsbrad|

R Tarren Mill
T Time to Strike
f Grab flight point
A Helcular's Revenge (Part 1) |N|From Novice Thaivand (63,19)|
A Elixir of Pain

C Helcular's Revenge (Part 1) |N|In the yeti cave (46,32)|

T Helcular's Revenge (Part 1) |N|Back in Tarren Mill|
A Helcular's Revenge (Part 2)
N Flame of Azel (Part 2) |N|Charge Flame of Azel inside yeti cave at (46,31)| |Q|Helcular's Revenge| |QO|Flame of Azel charged 1/1|
N Flame of Veraz |N|Charge Flame of Veraz| |Q|Helcular's Revenge| |QO|Flame of Veraz charged 1/1|

C Elixir of Pain |N|Kill mountain lions on the plateaus along the north edge of the zone.|
C Helcular's Revenge (Part 2) |N|Charge the Flame of Uzel in the Growless Cave (37,68) in Alterac Mountains| |Z|Alterac Mountains|
C Frostmaw |N|If you fail you can try again at level 37 since you won't turn this in until level 43|

T Helcular's Revenge (Part 2) |N|Drive rod into Helcular's grave at the Southshore graveyard (52.7, 53.3)|
T Elixir of Pain |N|Back in Tarren Mill|
A The Hammer May Fall
]]
end)
