TourGuide:RegisterGuide("Thousand Needles (25-26)", "Ashenvale (26-27)", "Horde", function()
    return [[
R Freewind Post
T Message to Freewind Post
A Pacify the Centaur
A Wanted - Arnak Grimtotem
A A Different Approach
A Alien Egg
A Wind Rider
f Freewind Post

C Pacify the Centaur
A Test of Faith |N|Cave Northeast of Freewind Post (52,43)|
C Test of Faith |N|*WALK* off the platform, do *NOT* jump.|
T Test of Faith |N|Do not accept "Test of Endurance" unless you're a hardcore masochist|
C A New Ore Sample
C Alien Egg |N|Look for occamy nests Southeast of Freewind Post|
C A Different Approach |N|Rock Elementals near the Shimmering Flats|
G Level 26 |N|Or two bars from it|

R Freewind Post
T Pacify the Centaur
A Grimtotem Spying
T Alien Egg
A Serpent Wild

H Camp Taurajo
A Washte Pawne |U|5103| |O|
T A New Ore Sample

F Thunder Bluff |N|For training|
t Abilities/Spells
T Melor Sends Word
A Steelsnap |N|Hunter Rise|
A The Sacred Flame |N|NPC is in the area connecting Elder and Hunter Rises|]]
end)

