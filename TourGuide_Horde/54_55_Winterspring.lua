TourGuide:RegisterGuide("Winterspring (54-55)", nil, "Horde", function()
    return [[
R Winterspring |N|Stealth, death-flop, grind (if you really really dont want Timbermaw goodies)|
N TBC]]
end)
--[[
54-55 Winterspring
01) Exit the cave and accept "Winterfall Activity" right outside of it.
02) Go to Donova Snowden (at (31,46))
03) Turn in "The New Springs" and "It's a Secret to Everybody" ... accept "Strange Sources" and "Threat of the Winterfall"
04) NOTE: I SKIP "The Videre Elixir" and the rest of that chain.
05) Go to Everlook..
06) Accept: "Are We There, Yeti?" "The Everlook Report" "Duke Nicholas Zverenhoff" "Sister Pamela"
07) Turn in "Felnok Steelspring" ... accept "Chillwind Horns"
08) Make Everlook your home.
09) Go Discover Darkwhisper Gorge for the quest "Strange Sources"
10) Then hearth back to Everlook.
11) Then go do the following quests:
12) "Are We There, Yeti?"
13) "Threat of the Winterfall" you should find "Empty Firewater Flask" which starts "Winterfall Firewater" while doing this quest.
14) "Wild Guardians"
15) "Chillwind Horns"
16) Once "Threat of the Winterfall" and "Winterfall Firewater" is completed go turn them in ... accept "Falling to Corruption"
17) Once the rest of those quests are complete, go back to Everlook and...
18) ...Turn in all quests, accept new ones (Except i SKIP "Return to Tinkee")
19) Then go do:
20) "Winterfall Activity"
21) "Are We There, Yeti?" (find 2 Pristine Yeti Horns)
22) Then hearth to Everlook..
23) Go turn in "Are We There, Yeti?" ... accept "Are We There, Yeti?" part3
24) Go scare Legacki with the mechanical yeti for the quest "Are We There, Yeti?" part3
25) Then fly to Felwood...
55-55 Felwood
01) Turn in "Wild Guardians" ... accept "Wild Guardians" part2
02) Go turn in "Cleansing Felwood" ... then get a cenarion beacon for the quest "Salve Via Hunting"
03) Go turn in "Verifying the Corruption" and "Forces of Jaedenar" ... accept "Collection of the Corrupt Water"
04) Go do in the following order:
05) "A Husband's Last Battle"
06) "Well of Corruption" collect 6 "Corrupted Soul Shard" for the quest "Salve Via Hunting"
07) "Collection of the Corrupt Water"
08) Then go to Bloodvenom Post
09) Turn in "A Husband's Last Battle" and "Well of Corruption" ... accept "Corrupted Sabers"
10) Go turn in "Salve Via Hunting" (the 6 Corrupted Soul Shards)
11) Go turn in "Collection of the Corrupt Water" ... I SKIP "Seeking Spiritual Aid"
12) Go do "Corrupted Sabers" then go turn it in.
13) Then go up north and do:
14) "Deadwood of the North"
15) "Falling to Corruption" (hint: I have my pet distract the mobs, while doing this quest)  (See Video) ... then accept "Mystery Goo"
16) Go turn in "Deadwood of the North"
17) Then fight your way through the cave..
18) Turn in "Winterfall Activity"
19) Then go turn in "Mystery Goo" ... I SKIP "Toxic Horrors"
20) Then hearth to Everlook.
21) Fly to Orgrimmar..
22) Make Orgrimmar your home.
23) Fly to Camp Mojache, Feralas.
24) Turn in "The Strength of Corruption"
25) Then fly to Tanaris.
26) Go scare Sprinkle with the mechanical yeti for the quest "Are We There, Yeti?" part3
27) Then fly to Silithus...
55-55 Silithus
01) Accept: "Report to General Kirika" "The Twilight Mystery" "Securing the Supply Lines" and "Deadly Desert Venom"
02) Go do: "Securing the Supply Lines" and "Deadly Desert Venom" (do these 2 quests northeast of Cenarion Hold)
03) Then turn them in and accept new quests.
04) Then go do:
05) "Stepping Up Security"
06) "The Twilight Mystery"
07) Go turn in "Report to General Kirika" at (50,69) ... accept "Scouring the Desert"
08) Go do "Noggle's Last Hope" along with "Scouring the Desert" Once you find the Silithyst item which looks like a glowing red thing, bring it back to the PVP horde base and stand in the teleporter looking thing, then turn the quest in for 6,600 XP!
09) "Wanted - Deathclasp, Terror of the Sands" (Elite) This quest is very easy to solo at lvl 55 (See Video).  If your class has troubles, either find a group to help, or you can simply skip it.
10) Once all of those are done, go turn them in at Cenarion Hold, accept new ones.
11) Go do: "Noggle's Lost Satchel" (it's at around (40,90))  (See Video)
12) "The Deserter" (he's in the cave at (67,71)) ... i SKIP "The Twilight Lexicon"
13) Then I die on purpose, so I end up at Cenarion Hold.
14) Turn in "Noggle's Lost Satchel"
15) Then go to Marshal's Refuge in Un'Goro Crater.
16) Go scare Quixxil with the mechanical yeti for the quest "Are We There, Yeti?" part3
17) Then hearth to Orgrimmar.
18) Go to the Under City.
19) Go complete "A Sample of Slime..." and "... and a Batch of Ooze"
21) Make Under City your home.
22) Go to the Bulwark... (which is a small village east of Tirisfal Glades)

]]--
