TourGuide:RegisterGuide("Eversong Woods (5-13)", "Ghostlands (13-20)", "Horde", function()
return [[
R Dawning Lane
T Aiding the Outrunners
A Slain by the Wretched
T Slain by the Wretched
A Package Recovery
T Package Recovery
A Completing the Delivery

R Falconwing Square
A Major Malfunction
A Unstable Mana Crystals
A Wanted: Thaelis the Hungerer |N|On a wanted poster (48,46)|
T Completing the Delivery
h Falconwing Square
t Train First Aid |N|Upstairs in the inn|

C Major Malfunction
C Unstable Mana Crystals
C Wanted: Thaelis the Hungerer

T Major Malfunction
T Unstable Mana Crystals
T Wanted: Thaelis the Hungerer
A Delivery to the North Sanctum
A Darnassian Intrusions
t Train (Level 6) |N|If you can|

R North Sanctum
A Roadside Ambush |N|Down the path (45,56)|
T Delivery to the North Sanctum
A Malfunction at the West Sanctum

T Malfunction at the West Sanctum |N|To the west (37,57)|
A Arcane Instability
C Arcane Instability
C Darnassian Intrusions
T Arcane Instability
T Darnassian Intrusions

A Fish Heads, Fish Heads... |N|Follow the path west (30,58)|
C Fish Heads, Fish Heads...
T Fish Heads, Fish Heads...
A The Ring of Mmmrrrggglll
A Grimscale Pirates! |N|South of West Sanctum (36,67)|
A Captain Kelisendra's Lost Rutters |U|21776| |O|
T Captain Kelisendra's Lost Rutters |O|
A Lost Armaments

R Fairbreeze Village |N|Follow the road East|
A Pelt Collection
A The Wayward Apprentice
A Situation at Sunsail Anchorage
h Fairbreeze Village

T Roadside Ambush |N|To the north|
A Soaked Pages
C Soaked Pages |N|The Grimoire is in the water just south west of that bridge|
T Soaked Pages
A Taking the Fall

R Falconwing Square
A Incriminating Documents |U|20765| |O|
T Incriminating Documents |O|
A The Dwarven Spy
t Train (Level 6) |N|Train level 8 if you can|

C The Dwarven Spy |N|Near North Sanctum|

R Falconwing Square
T The Dwarven Spy |N|Falconwing Square|
t Train (Level 8)

A The Dead Scar |N|To the Southeast (50,51)|
C The Dead Scar |N|Slightly to the South|
T The Dead Scar
T Taking the Fall |N|Stillwhisper Pond to the East|
A Swift Discipline

R Farstrider Retreat |N|Follow the road East then South|
A Amani Encroachment
A The Spearcrafter's Hammer
A The Magister's Apprentice

T The Wayward Apprentice |N|To the Southwest|
A Corrupted Soil
C Corrupted Soil
T Corrupted Soil
A Unexpected Results
C Unexpected Results
T Unexpected Results
A Research Notes
G Until level 9

H Fairbreeze Village
A The Scorched Grove
A Ranger Sareyn
T Research Notes
A Saltheril's Haven

N Kill any cats you come across for "Pelt Collection"
R Saltheril's Haven |N|To the west|
T Saltheril's Haven
A The Party Never Ends

G Grind your way west to the shore
C The Ring of Mmmrrrggglll |N|Use your racial to stop Mmmrrrggglll from healing himself!|
C Grimscale Pirates!
T The Ring of Mmmrrrggglll

R Sunsail Anchorage |N|To the South|
C Lost Armaments
C Situation at Sunsail Anchorage
T Lost Armaments
T Grimscale Pirates!
A Wretched Ringleader
C Wretched Ringleader |N|The mob is at the very top of building at Sunsail Anchorage.  Focus fire on him, grab his head and run out of there!|
T Wretched Ringleader
C Pelt Collection

R Fairbreeze Village
T Pelt Collection
T Situation at Sunsail Anchorage
A Missing in the Ghostlands
T Ranger Sareyn |N|To the East along the road (47,71)|
A Defending Fairbreeze Village

N You should be level 10 now |N|Time to do your class's level 10 quests!|
T Missing in the Ghostlands
A The Fallen Courier
C The Fallen Courier
T The Fallen Courier
A Delivery to Tranquillien
C Defending Fairbreeze Village |N|Southern half of the Dead Scar|
A Powering our Defenses |N|At the runestone (42,85)|
C Powering our Defenses |N|At the East runestone (55,84)|
C Swift Discipline |N|North of Fairbreeze Village along the road.  Meledor (44,60) and Ralen (45,56)|
T Swift Discipline

R Silvermoon City
B [Suntouched Special Reserve] |N|Vinemaster Suntouched (80,58), for "The Party Never Ends"|
t Train (Level 10)

R Eversong Woods
T The Magister's Apprentice |N|Follow the road East (67,57)|
A Deactivating the Spire
A Where's Wyllithen?
C Deactivating the Spire |N|Keep an eye out for Magister Duskwither's Journal on your way up.|
A Abandoned Investigations
T Where's Wyllithen? |N|To tho North (68,46)|
A Cleaning up the Grounds
C Cleaning up the Grounds
T Cleaning up the Grounds
T Deactivating the Spire
A Word from the Spire

R Farstrider Retreat
T Word from the Spire
T Abandoned Investigations
C The Spearcrafter's Hammer |N|To the southeast (70,72)|
A Zul'Marosh
C Zul'Marosh |N|Across the lake, upstairs in the hut (62, 80)|
T Zul'Marosh
C Amani Encroachment

R Farstrider Retreat
B [Springpaw Appetizers] |N|From Zalene Firstlight (60,63), for "The Party Never Ends"|
T The Spearcrafter's Hammer
T Amani Encroachment
A Amani Invasion |U|23249| |O|
T Amani Invasion |O|
A Warning Fairbreeze Village

H Fairbreeze Village
T Warning Fairbreeze Village
B [Bundle of Fireworks] |N|From the General goods vendor, for "The Party Never Ends"|
T Defending Fairbreeze Village |N|(47,72)|

T The Party Never Ends |N|The quest reward item, [Saltheril's Haven Party Invitation], just allows you to pick up the party goods on the tables at the party.|
T The Scorched Grove |N|(34,80)|
A A Somber Task
C A Somber Task |N|Make sure you kill Old Whitebark for his pendant|
A Old Whitebark's Pendant |N|Started by the item, of course|
T A Somber Task
T Old Whitebark's Pendant
A Whitebark's Memory
C Whitebark's Memory |N|Bury the pendant (37,86) and fight his spirit to low health|
T Whitebark's Memory
T Powering our Defenses
]]
end)

