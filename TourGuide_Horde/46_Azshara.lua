TourGuide:RegisterGuide("Azshara (46)", "Hinterlands (46-47)", "Horde", function()
return [[
F Thunder Bluff
T Deadmire |N|Hunter Rise (61,80)| |Z|Thunder Bluff|

F Splintertree Post
R Azshara
A Spiritual Unrest |N|To the south of the road as you enter the zone (11.4,78.2)|
A A Land Filled with Hatred
C Spiritual Unrest |N|Ghosts in the ruins north of the road (17,66)|
C A Land Filled with Hatred |N|Further north, more ruins containing satyrs (20,62)|
T Spiritual Unrest
T A Land Filled with Hatred

R Valormok |N|North of the road, on the mountain's edge (21,52)|
T Betrayed

F Undercity |N|Fly to Orgrimmar and take the zeplin, of course|
A Lines of Communication |N|In the Magic quarter of Undercity|
]]
end)
