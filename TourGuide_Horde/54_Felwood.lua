TourGuide:RegisterGuide("Felwood (54)", "Winterspring (54-55)", "Horde", function()
    return [[
F Splintertree Post
R Felwood
A Forces of Jaedenar |N|(50,81)
A Verifying the Corruption |N|Elite|
A Cleansing Felwood |N|(46,84)|
N Kill Oozes when you see them, you need at least 30 samples for "A Sample of Slime..."
C Forces of Jaedenar
R Bloodvenom Post
f Bloodvenom Post
A Well of Corruption
A A Husbund's Last Battle
A Wild Guardians
C Verifying the Corruption
C Cleansing Felwood
C The Strength of Corruption
A Deadwood of the North |N|But don't do it yet (64,8)|]]
end)

