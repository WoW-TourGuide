TourGuide:RegisterGuide("UnGoro Crater (51-52)", "Burning Steppes (52-53)", "Horde", function()
    return [[
N Get a Mithril Casing if you want to escort the mechanical ape.
R Un'Goro Crater
A The Apes of Un'Goro |N|Tauren on the hill to the left of the ramp from Tanaris (69,77)|
A The Fare of Lar'korwi
C The Fare of Lar'korwi |N|Click on a carcass between the eastern lakes and the river (68,56)|
N Do "Super Sticky" until "A Mangled Journal" drops
A Williden's Journal |U|11116|
A Chasing A-Me 01 |N|Outside Marshal's Refuge|

R Marshal's Refuge
A Shizzle's Flyer
A Lost!
A Beware of Pterrordax
A Roll the Bones
A Alien Ecology
T Williden's Journal
A Expedition Salvation
A Larion and Muigin |N|Hidden on the north side of the valley|
A Crystals of Power
f Marshal's Refuge

N While you're questing, kill oozes when you see them and loot Bloodpetal Pods
C Super Sticky
C The Apes of Un'Goro |N|Apes call for help! Expect adds.|
C Chasing A-Me 01 |N|Inside the cave, up the right hand tunnel|
C Larion and Muigin
C Beware of Pterrordax |N|East of Marshal's Refuge (56,8)|
C Roll the Bones
C Expedition Salvation
T The Apes of Un'Goro
A The Mighty U'cha
T The Fare of Lar'korwi
A The Scent of Lar'korwi
C Bone-Bladed Weapons
C The Scent of Lar'korwi
A It's a Secret to Everybody (Part 1) |N|Click the bag in the water at (63,69) after getting the quest from the boat|
C Alien Ecology
C Bungle in the Jungle

A Finding the Source |N|Near the Western Lakes (30,51)|
A Volcanic Activity
C Finding the Source |N|The hotspot is at (50,46), the path to hotspot starts on the north face. (51,42)|
C Volcanic Activity
C Lost! |N|Jump down from the hotspot plateau to the second cave.|
T Lost!
A A Little Help from My Friends

R Marshal's Refuge |N|Escort him to Marshal's Refuge, hotkey the Canteen|
T Shizzle's Flyer
T A Little Help from My Friends
T Chasing A-Me 01
T Beware of Pterrordax!
T Roll the Bones
T Alien Ecology
T Expedition Salvation
T Larion and Muigin
T It's a Secret to Everybody (Part 1)
A It's a Secret to Everybody (Part 2)
T Crystals of Power
A The Northern Pylon
A The Eastern Pylon
A The Western Pylon

N Turn in any other quests?
N Accept New Quests

C The Northern Pylon |N|Pylon is east of Marshal's Refuge (56,13)|
C The Mighty U'cha
C The Eastern Pylon |N|Pylon at (77,50)|
C The Bait for Lar'korwi
T The Bait for Lar'korwi
T The Mighty U'cha

T Finding the Source |N|Grind westward towards (30.9,50.4), kill oozes as you go|
A The New Springs
C The Western Pylon |N|Pylon at (23,58)|

R Marshal's Refuge |N|Kill more oozes along the way if you have less then 30 ooze samples, otherwise run to Silithus and get FP then fly back to Marshal's Refuge|

C The Northern Pylon
C The Eastern Pylon
C The Western Pylon
A Making Sense of It
C Making Sense of It

N Make sure you have at least 30 ooze samples for "... and a Batch of Ooze"

R Silithus
R Cenarion Hold
f Cenarion Hold

F Gadgetzan
T Super Sticky
T Bungle in the Jungle

F Thunder Bluff
R Elder Rise
A Un'goro Soil
T Un'goro Soil
A Morrowgrain Research
T Morrowgrain Research
t Train!

H The Crossroads
F Ratchet
T Volcanic Activity
T Marvon's Workshop]]
end)

