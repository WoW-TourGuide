TourGuide:RegisterGuide("Ghostlands (13-20)", nil, "Horde", function()
return [[
R Tranquillien |N|Follow the road south|
T Delivery to Tranquillien
A The Forsaken
T The Forsaken
A Return to Arcanist Vandril
T Return to Arcanist Vandril
A Suncrown Village
A Tomber's Supplies
A Goods from Silvermoon City
f Grab the flight point
T Goods from Silvermoon City
h Tranquillien
A The Plagued Coast

A Suncrown Village |N|Northeast|
A Anok'suten |N|(57,14)|
C Suncrown Village
C Anok'suten
A Dealing with Zeb'Sora |N|To the east (69,15)|
A Forgotten Rituals |N|On the island (72,19)|

R Farstrider Enclave |N|To the south (72,32|
A Bearers of the Plague
A Spirits of the Drowned

N Swim north |N|Work on "Spirits of the Drowned" and "Forgotten Rituals" along the way|
C Dealing with Zeb'Sora |N|East side of the lake (74,12)|
T Dealing with Zeb'Sora
A Report to Captain Helios
C Forgotten Rituals
T Forgotten Rituals
A Vanquishing Aquantion
C Vanquishing Aquantion |N|(71,14)|
T Vanquishing Aquantion
C Spirits of the Drowned
C Bearers of the Plague

R Farstrider Enclave
T Report to Captain Helios
T Spirits of the Drowned
T Bearers of the Plague
A Curbing the Plague

H Tranquillien
A Culinary Crunch
A Salvaging the Past
A Down the Dead Scar
A Investigate An'daroth
A Trouble at the Underlight Mines
T Suncrown Village
T Anok'suten
A Goldenmist Village

C Down the Dead Scar |N|To the west|
C Salvaging the Past |N|Around (34,34)|
C Tomber's Supplies |N|In a cart (33,26)|
C Investigate An'daroth |N|Around (37,15)|
C Goldenmist Village
C The Plagued Coast |N|On the coast west of Goldenmist Village|
C Curbing the Plague |N|Down the coast (24,38)|
C Culinary Crunch |N|Kill more spiders if you didn't get enough|

A Underlight Ore Samples |N|Grind mobs on your way (31,48)|
C Trouble at the Underlight Mines
C Underlight Ore Samples

R Tranquillien
T Down the Dead Scar
T Trouble at the Underlight Mines
T Culinary Crunch
T Tomber's Supplies
T Investigate An'daroth
T Salvaging the Past
T Goldenmist Village
T The Plagued Coast
A Into Occupied Territory
A Investigate the Amani Catacombs
A Retaking Windrunner Spire
A Troll Juju
A Spinal Dust
A Rotting Hearts
A Windrunner Village

C Into Occupied Territory |N|In tents (14,27), (13,25), and on the boat (11,22)|
C Windrunner Village |N|(18,43)|
C Retaking Windrunner Spire |N|(16,60)|
A The Lady's Necklace |U|22597| |O|

H Tranquillien
T Retaking Windrunner Spire
T Into Occupied Territory
T Windrunner Village
A Deliver the Plans to An'telas

R Sanctum of the Sun |N|To the southeast (55,48)|
T Underlight Ore Samples
A The Farstrider Enclave

T Deliver the Plans to An'telas |N|(60,35)|
A Deactivate An'owyn

R Farstrider Enclave
T Curbing the Plague
T The Farstrider Enclave
A Attack on Zeb'Tela
A Shadowpine Weaponry
A A Little Dash of Seasoning
A The Traitor's Shadow
A Bring Me Kel'gash's Head! |N|On a wanted poster.  This is an elite quest that requires a group!|

T The Traitor's Shadow |N|At the dusty journal in the building at the top of the ramp (80,18)|
A Hints of the Past
C Attack on Zeb'Tela
T Attack on Zeb'Tela
A Assault on Zeb'Nowa
T Hints of the Past
A Report to Magister Kaendris

C Troll Juju |N|West of the enclave (66,29).  Work your way to the questgiver inside in a cage (63,33)|
C Investigate the Amani Catacombs
A Escape from the Catacombs
C Escape from the Catacombs
T Escape from the Catacombs

R Zeb'Nowa |N|South (68,51)|
C A Little Dash of Seasoning |N|Fish rack (68,57), raw meat rack (65,66), smoked meat rack (63,75n)|
C Shadowpine Weaponry
C Assault on Zeb'Nowa
C Bring Me Kel'gash's Head! |N|He's elite, if you can't solo him get a group or come back later (65,79)| |O|
C Deactivate An'owyn

R Farstrider Enclave
T Bring Me Kel'gash's Head! |O|
T Assault on Zeb'Nowa
T A Little Dash of Seasoning
T Shadowpine Weaponry
T Deactivate An'owyn |N|(60,35)|

R Sanctum of the Sun
T Report to Magister Kaendris
A The Twin Ziggurats
A War on Deatholme
A Clearing the Way |N|(46,56)|
C Clearing the Way
T Clearing the Way

C Spinal Dust
C Rotting Hearts
C The Twin Ziggurats
C War on Deatholme

H Tranquillien
T Troll Juju
T Investigate the Amani Catacombs
T Rotting Hearts
T Spinal Dust

R Sanctum of the Sun
T War on Deatholme
T The Twin Ziggurats
A The Traitor's Destruction
A Dar'Khan's Lieutenants
A A Restorative Draught

R Tranquillien
T A Restorative Draught
A Captives at Deatholme

R Deatholme
U Drink up! |U|22779|
U Drink up! |U|22778|
C Captives at Deatholme |N|Enith is in the crypt (32,73), Varnis (40,83), Vedoran (32,89)|
C Dar'Khan's Lieutenants |N|Jurion is in the crypt (32,73), Borgoth (40,83), Masophet (36,88), Mirdoran (37,79)|
C The Traitor's Destruction |N|He's a tough elite, so if you don't have a group you can skip this| |O|
G Grind until ~15% to 20 |N|That's 3 "bubbles"|

R Sanctum of the Sun |N|Or die and spirit rez|
T The Traitor's Destruction |O|
A Hero of the Sin'dorei |N|If you finished "The Traitor's Destruction"|
T Dar'Khan's Lieutenants
T Captives at Deatholme

R Tranquillien
A Fly to Silvermoon City
F Silvermoon City
t Train (First Aid)
t Train (Level 20)
T Fly to Silvermoon City |N|(53,71)|
T Hero of the Sin'dorei |N|(53,20)| |O|

R Undercity |N|Teleport an the sunspire (49,14)|

]]
end)

--~ 51) Then get on the zeppelin to go to Orgrimmar
--~ 52) Get FP in Orgrimmar
--~ 53) Then run all the way to the XRs (Crossroads) in the Barrens...

