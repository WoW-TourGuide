TourGuide:RegisterGuide("The Barrens (12-20)", "Stonetalon Mountains (15-16)", "Horde", function()
    return [[
T Conscript of the Horde |N|Just over the bridge from Durotar to The Barrens|
A Crossroads Conscription
T Ak'Zeloth |N|Skip "The Demon Seed"|

N Tame a Savannah Huntress while running to the Crossroads |C|Hunter|
R The Crossroads |N|Follow the road, taking left turn to the Crossroads (52,30)|
T Crossroads Conscription
A Plainstrider Menace
A The Forgotten Pools
A Raptor Thieves
h The Crossroads
A Disrupt the Attacks
A Supplies for the Crossroads
A Harpy Raiders |N|On the watch tower|
f The Crossroads
A Wharfmaster Dizzywig
A Fungal Spores
C Disrupt the Attacks |N|East of XR, north of the mountain. Kill Plainstriders and Raptors as you see them. (54,26)|
T Disrupt the Attacks
A The Disruption Ends
C The Disruption Ends |N|East of XR, north of the mountain (56,26) Keep an eye out for boxes of Supplies.|
C Supplies for the Crossroads
C Plainstrider Menace
C Raptor Thieves

R Ratchet
A Chen's Empty Keg |U|4926| |O|
A Raptor Horns
A WANTED: Baron Longshore |N|From the sign outside the bank|
A Samophlange (Part 1)
f Ratchet
A Southsea Freebooters |N|From Gazlowe|
T Wharfmaster Dizzywig
A Miner's Fortune
A The Guns of Northwatch
C Southsea Freebooters
C WANTED: Baron Longshore
T Southsea Freebooters
A The Missing Shipment (Part 1)
T WANTED: Baron Longshore
T The Missing Shipment (Part 1)
A The Missing Shipment (Part 2)
T The Missing Shipment (Part 2)
A Stolen Booty
C Stolen Booty
H The Crossroads
T Supplies for the Crossroads
T The Disruption Ends
T Plainstrider Menace
A The Zhevra
T Raptor Thieves

A Kolkar Leaders |N|Run west of the Crossroads (45.4,28.4)|
A Centaur Bracers
C Kolkar Leaders |N|Barak is at (43,24). Do "The Zhevra", "Raptor Thieves", "The Forgotten Pools" and "Centaur Bracers" as seen.|
C Fungal Spores
C The Forgotten Pools
C Harpy Raiders |N|Northwest corner of the Barrens (38,17)|
R Honor's Stand |N|West of Crossroads|
A Goblin Invaders
A Avenge My Village
R Stonetalon Mountains
C Avenge My Village
T Avenge My Village
A Kill Grundig Darkcloud
R Stonetalon Mountains
C Kill Grundig Darkcloud |N|Grimtotem camp north of the little troll village (73,86)|
T Kill Grundig Darkcloud

G Grind to the Kolkar quest guy |N|West of XR (45.4,28.4)|
T Kolkar Leaders
A Verog the Dervish
C Centaur Bracers
C Raptor Thieves
C The Zhevra
R The Crossroads
T Fungal Spores
T The Zhevra
A Prowlers of the Barrens
T The Forgotten Pools
A The Stagnant Oasis
N Turn in quests, grab new ones...
C Apothecary Zamah |N|Run to Thunder Bluff|
C Lost in Battle |N|South of Crossroads after the bridge (49.3,50.4)|
R Camp Taurajo
A Tribes at War
f Camp Taurajo
N Get "Melor Sends Word" if needed.
R Mulgore
R Thunder Bluff |N|Thunder Bluff (37.0,31.8)|
t Weapon Master |N|(40,62)|
t Class Training
T Apothecary Zamah |N|Cave below Spirit Rise (22.9,20.9)|
t First Aid |N|First Aid Trainer (29.6,21.3)|
f Thunder Bluff
A The Sacred Flame (Part 1) |N|(55.1,53.1)|
H The Crossroads
T Lost in Battle
C Prowlers of the Barrens |N|(37,20)|
C Harpy Lieutenants |N|Kill Witchwing Slayer's - Northwest Barrens (38,14)|
T Samophlange (Part 1)
A Samophlange (Part 2)
T Samophlange (Part 2)
A Samophlange (Part 3)
C Samophlange (Part 3) |N|Tinkerer is in the hut on the hill|
T Samophlange (Part 3)
A Samophlange (Part 4)
C Ignition
C The Escape
C Miner's Fortune
C The Stagnant Oasis
C Verog the Dervish
R Ratchet
T Miner's Fortune
T Samophlange (Part 4)
F The Crossroads
T The Stagnant Oasis
A Altered Beings
T Prowlers of the Barrens
A Echeyakee
A Stolen Silver
A Report to Kadrak
A Serena Bloodfeather
T Harpy Lieutenants
N Accept New Quests
T Centaur Bracers
T Verog the Dervish
C Serena Bloodfeather |N|Serena Bloodfeather - Northwest Barrens (38,11)|
C Echeyakee
H The Crossroads
T Echeyakee
A The Angry Scytheclaws
T Serena Bloodfeather
A Letter to Jin'Zil
A Consumed by Hatred
C Altered Beings |N|(55,42)|
C The Angry Scytheclaws |N|(51,46)|
C Raptor Horns
C Consumed by Hatred |N|(51,54)|
G Camp Taurajo
A Weapons of Choice
T Tribes at War
A Blood Shards of Agamaggan
T Blood Shards of Agamaggan
A Betrayal from Within (Part 1)
T Spirit of the Wind |N|10 Blood Shards, repeatable|
N Grind to one bar from Level 20 (or group for Wailing Caverns)
T The Angry Scytheclaws
A Jorn Skyseer
T Consumed by Hatred
T Altered Beings
A Egg Hunt
F Orgrimmar
t Level 20
A The Ashenvale Hunt
H The Crossroads
F Ratchet
T Raptor Horns
A Deepmoss Spider Eggs
A Ziz Fizziks
C The Guns of Northwatch
C Stolen Silver
C Free From the Hold |N|Escort Quest|
R Ratchet
T The Guns of Northwatch
T Free From the Hold
H The Crossroads
T Stolen Silver]]
end)

