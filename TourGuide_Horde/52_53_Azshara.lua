TourGuide:RegisterGuide("Azshara (53-54)", "Felwood (54-54)", "Horde", function()
    return [[
H The Crossroads |N|Or whatever method is fastest to get to Orgrimmar|
F Orgrimmar
h Orgrimmar
F Azshara
A Betrayed (Part 1)
T The Hunter's Charm |C|Hunter| |N|(41,42)|
A Courser Antlers |C|Hunter|
R Legash Encampment |N|Northwest of FP, North of the cave in the Bay of Storms (53,21)|
A Kim'jael Indeed! |N|At the top of the hill (53.5,21.8)|
C Betrayed |N|Directly south of the goblin Kim'jael|
T Betrayed (Part 1) |N|Click on the book in the pagoda (59.5,31.2)|
A Betrayed (Part 2) |N|This will spawn a 51 warrior type, then the magus|
C Betrayed (Part 2)
C Courser Antlers |C|Hunter|
C Kim'jael Indeed!
T Kim'jael Indeed!
A Kim'jael's "Missing" Equipment
T Courser Antlers |C|Hunter|
A Wavethrashing |C|Hunter|
C Kim'jael's "Missing" Equipment
C Wavethrashing |C|Hunter|
H Orgrimmar |N|Or run back to FP if Hearthstone is on cooldown|
F Azshara
T Betrayed (Part 3)
A Betrayed (Part 4)
T Wavethrashing |C|Hunter|
T Kim'jael's "Missing" Equipment
G Grind Blood Elves to Level 54
H Orgrimmar
T Bone-Bladed Weapons
T Betrayed (Part 4)]]
end)

