TourGuide:RegisterGuide("Stranglethorn Vale (47)", nil, "Horde", function()
return [[
b Grom'gol Base Camp
T Grim Message
T The Singing Crystals

F Booty Bay
A The Bloodsail Buccaneers (Part 5)
A Whiskey Slim's Lost Grog
h Booty Bay
T Back to Booty Bay
A The Captain's Chest
C The Captain's Chest |N|On the beach east of Booty Bay, head north when you reach the sea (36.3,69.7)|
A Message in a Bottle |N|Loot the bottles on the beach, quest starts from Carefully Folded Note|
T Message in a Bottle |N|On the large island east of Booty Bay (38.5,80.6)|
C The Bloodsail Buccaneers (Part 5) |N|In the three ships off the coast|
A Cortello's Riddle |N|Check downstairs in the ships|

H Booty Bay
T The Captain's Chest
T The Bloodsail Buccaneers (Part 5)
N Before leaving Booty Bay, get a stack of Silk Cloth from the mail or bank
]]
end)
