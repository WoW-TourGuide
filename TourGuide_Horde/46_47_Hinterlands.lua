TourGuide:RegisterGuide("The Hinterlands (46-47)", "Stranglethorn Vale (47)", "Horde", function()
return [[
F Tarren Mill
h Tarren Mill
R The Hinterlands

T Ripple Recovery |N|In the hills above Quel'Danil Lodge (26.7,48.5)|
A A Sticky Situation

R Revantusk Village |N|follow the road east to the coast, Revantusk is at the south end (76.9,77.5)|
A Vilebranch Hooligans
A Cannibalistic Cousins
A Message to the Wildhammer
A Stalking the Stalkers
A Hunt the Savages
A Avenging the Fallen

C Vilebranch Hooligans
C Cannibalistic Cousins
C A Sticky Situation |N|In the cave at Skulk Rock (57,41)|
C Stalking the Stalkers
C Hunt the Savages
C Testing the Vessel
C Avenging the Fallen
C Lines of Communication
C Message to the Wildhammer
C Rin'ji is Trapped! |N|He's in a cage in Quel'Danil Lodge (30.7,47.0)|
A Venom Bottles |N|You will find bright green bottles lying around at the Witherbark Troll villages, loot one to start the quest (31.6,57.8)|
C Grim Message

T A Sticky Situation |N|In the hills above Quel'Danil Lodge (26.7,48.5)|
A Ripple Delivery

A Find OOX-09/HL! |O| |L|8704|
T Find OOX-09/HL! |N|On a small island in the lake west of Skulk Rock (49.4,37.7) Skip the escort|

T Rin'ji is Trapped! |N|On the small island off the coast, southeast of the point where the river meets the sea (86.3,59.1)|
A Rin'ji's Secret

T Vilebranch Hooligans |N|Back in Revantusk Village|
T Cannibalistic Cousins
T Message to the Wildhammer
T Stalking the Stalkers
T Hunt the Savages
T Avenging the Fallen

t Train First Aid |N|Fly to Hammerfall and train if you need to|

F Tarren Mill
T Venom Bottles
A Undamaged Venom Sac

R The Hinterlands |N|The next quests are done in the western part of the zone, so don't be tempted to fly to Revantusk Village|
C Undamaged Venom Sac
C The Atal'ai Exile |N|Climb the south temple and look down off the stairs to your left. You should be able to see the exile on the platform (33.7,75.2)|
A Return to Fel'Zerul

H Tarren Mill
T Undamaged Venom Sac
F Undercity

N Donate cloth |N|You know the drill, 60 Mageweave in the Magic Quarter|
T Lines of Communication
T Rin'ji's Secret |N|Don't forget to talk to her again for a reward|
A Seeping Corruption |N|In the Apothecarium|
A Errand for Apothecary Zinge (Part 1)
T Errand for Apothecary Zinge (Part 1)
A Errand for Apothecary Zinge (Part 2)
T Errand for Apothecary Zinge (Part 2)
A Into the Field
N Bank the Field Testing Kit and Box of Empty Vials
]]
end)
