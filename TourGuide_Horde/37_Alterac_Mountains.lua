TourGuide:RegisterGuide("Alterac Mountains (37)", "Thousand Needles (37)", "Horde", function()
return [[
F Tarren Mill
A Prison Break In |N|From Magus Wordeen Voidglare (60,20)| |Z|Hillsbrad Foothills| |NODEBUG|
A Stone Tokens |N|From Keeper Bel'varil|

C Stone Tokens |N|At Dalaran (20,85)| |Z|Hillsbrad Foothills|
C Prison Break In |NODEBUG|
T Prison Break In |NODEBUG|
T Stone Tokens

A Dalaran Patrols
A Bracers of Binding
C Dalaran Patrols |N|Go to Dalaran again|
C Bracers of Binding

T Dalaran Patrols
T Bracers of Binding

F Undercity
T To Steal From Thieves |NODEBUG|
B Soothing Spices x3 |L|3713 3| |N|From Felicia Doan in the center of Undercity|
B Red-speckled Mushroom x5 |N|From Tawny Grisette in the center of Undercity. You will need this later| |C|Hunter| |L|4605 5|
]]
end)
