TourGuide:RegisterGuide("Ashenvale (29-30)", "Wetlands (30-31)", "Alliance", function()
return [[
F Astranaar |N|Fly to Menethil Harbor then take the boat to Auberdine.|
A A Helping Hand
h Astranaar

R Forest Song |N|Easternmost part of Ashenvale, Northeast from Satyrnaar|
T A Helping Hand
A A Shameful Waste
A Vile Satyr! Dryads in Danger!
A Destroy the Legion
A The Howling vale
A Agents of Destruction |N|Ignore her other quest, unless you plan to visit WSG.|
A Reclaiming Felfire Hill |N|From Gnarl the Ancient of War, he paths around the camp.|
T Kayneth Stillwind
A Forsaken Diseases
A The Lost Chalice
f Grab flight point |N|Up on the hill (85,43).|
T Vile Satyr! Dryads in Danger! |N|To the west of Forest Song (78,45)|
A The Branch of Cenarius

C The Branch of Cenarius |N|Kill Geltharis (78,42)|
T The Branch of Cenarius |N|Back in Forest Song.|
A Satyr Slaying!

C The Lost Chalice |N|In Satyrnaar (81,49)|
N Get wood |N|Collect 5 Satyrnaar Fel Wood.| |L|24081 5|
N Right click on the ground below the pink crystal. |N|(80.8,48.9)|
C Satyr Slaying!

T Satyr Slaying! |N|Back in Forest Song.|
T The Lost Chalice

C Agents of Destruction |N|South of Forest Song in Warsong Lumber Camp (88,54)|
C A Shameful Waste
C Destroy the Legion |N|Go southwest (81,67)|
C Reclaiming Felfire Hill
C Forsaken Diseases |N|Go southwest across the river (75,71)|
C Fallen Sky Lake |N|Kill the Oracle (66,81)|

R Night Run |N|North of Fallen Sky Lake. (65,55)|
C The Tower of Althalaxx |N|Right click on the ground below the pink crystal.|
C Raene's Cleansing |N|Kill treants for the key and open the box. (54,35)|
C The Howling Vale |N|Click on the book. (50,39)|
T Raene's Cleansing |N|Go south. (53,46)|
H Astranaar |N|Or run if you want.|

T Fallen Sky Lake
R Maestra's Post

T The Tower of Althalaxx |N|Ignore the next quest.|
F Forest Song |N|Run to Astranaar then fly.|

T A Shameful Waste
T Reclaiming Felfire Hill
T Destroy the Legion
T The Howling Vale
T Agents of Destruction
T Forsaken Diseases

A Diabolical Plans |U|23777| |O|
T Diabolical Plans |O|
]]
end)