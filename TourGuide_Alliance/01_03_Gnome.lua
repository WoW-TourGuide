TourGuide:RegisterGuide("Dun Morogh (Gnome)", "Dun Morogh (3-11)", "Alliance", function()
return [[
A Dwarven Outfitters
C Dwarven Outfitters
T Dwarven Outfitters

A Tainted Memorandum |C|Warlock|
A Glyphic Memorandum |C|Mage|
A Encrypted Memorandum |C|Rogue|
A Simple Memorandum |C|Warrior|

A Coldridge Valley Mail Delivery (Part 1)
A A New Threat
C A New Threat
T A New Threat

T Tainted Memorandum |C|Warlock|
T Glyphic Memorandum |C|Mage|
T Encrypted Memorandum |C|Rogue|
T Simple Memorandum |C|Warrior|
]]
end)