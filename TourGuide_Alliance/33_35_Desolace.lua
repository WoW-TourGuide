
TourGuide:RegisterGuide("Desolace (33-35)", nil, "Alliance", function()
return [[
F Stonetalon Peak |N|Fly to Menethil Harbor, boat to Auberdine, then fly down.|
A Reclaiming the Charred Vale
C Reclaiming the Charred Vale |N|South in the Charred Vale (32,66)| |Z|Stonetalon Mountains|

R Desolace |N|South, naturally|
R Nijel's Point |N|Along the norther edge of Desolace (64,10)|
A Vahlarriel's Search (Part 1)
A Centaur Bounty
T Reclaimers' Business in Desolace
A Reagents for Reclaimers Inc. (Part 1)
A The Karnitol Shipwreck (Part 1)
h Nijel's Point

F Stonetalon Peak
T Reclaiming the Charred Vale |N|Skip the follow-up|
H Nijel's Point

T Vahlarriel's Search (Part 1) |N|Find the chest in the broken down caravan to the west (56,17)|
A Vahlarriel's Search (Part 2)
C Reagents for Reclaimers Inc. (Part 1) |N|Kill Hatefury satyrs to the east at Sargeron (75,20)|

T Vahlarriel's Search (Part 2) |N|Back in Nijel's Point|
A Vahlarriel's Search (Part 3)
T Reagents for Reclaimers Inc. (Part 1)
A Reagents for Reclaimers Inc. (Part 2)

N Kill crap... |N|Kill any Scorpashi and Aged Kodo you come across|
A Bone Collector |N|To the south along the road at Kormek's Hut (62,38)|
C Centaur Bounty |N|To the east at the Kolkar Village (72,45)|
A Kodo Roundup |N|Grind your way to Scrabblescrew's Camp (60,61)|
C Kodo Roundup |U|13892| |N|Use the Kodo Kombobulator on Kodo in the graveyard, then take the kodo back to Scrabblescrew.|
T Kodo Roundup
C Bone Collector |N|Loot the kodo bones lying around the graveyard|

A Sceptre of Light |N|At the tower near the ocean to the northwest (38.9, 27.2)|
T Vahlarriel's Search (Part 3) |N|Turn this in to Dalinda Malem in the main building at the center of Thunder Axe Fortress (54,26)|
A Search for Tyranis
C Sceptre of Light |N|Kill Burning Blade Seers|
C Search for Tyranis |N|Found inside the building west (53.0,29.0)|
T Search for Tyranis |N|Back in the other building.  Follow-up is an escort so be ready.|
A Return to Vahlarriel |N|Dalinda may ignore mobs, so don't attack anything unless she attacks or is attacked|
C Return to Vahlarriel |N|Dalinda may ignore mobs, so don't attack anything unless she attacks or is attacked|

T Bone Collector |N|Back at Kormek's Hut|
T Sceptre of Light |N|Back at the tower by the shore|
A Book of the Ancients
T The Karnitol Shipwreck (Part 1) |N|At the shipwreck on the coast (36.1, 30.4)|
A The Karnitol Shipwreck (Part 2)
A Claim Rackmore's Treasure! |N|Found on a barrel beside the Karnitol Chest|
C Claim Rackmore's Treasure! |N|Kill Drysnap Pincers and Crawlers for the silver key, Slitherblade Naga for the golden key.|

T Claim Rackmore's Treasure! |N|Find the chest behind a tree on Ranazjar Isle (30.0, 8.7)|
C Book of the Ancients |N|Clear the area around the statue, then talk to it and kill the naga that spawns.|
T Book of the Ancients |N|Back on shore at the tower|

C Reagents for Reclaimers Inc. (Part 2) |N|Get the last few reagents you need, you should have most by now.|

H Nijel's Point

N Stable your pet |C|Hunter|
N Tame Scorpashi |N|Find a lv34, for Claw (Rank 5).  Found all around the Kodo Graveyard (58,50)| |C|Hunter|
N Keep this pet if you want |N|Otherwise get your pet out of stable| |C|Hunter|

T Reagents for Reclaimers Inc. (Part 2) |N|Skip the follow-up|
T The Karnitol Shipwreck (Part 2) |N|Skip the follow-up|
T Centaur Bounty
T Return to Vahlarriel

R Feralas |N|Follow the road to the southwest edge of the zone (43,98)|
R Feathermoon Stronghold |N|Follow the road until the path down to the docks.  Take the boat if it's there, or swim due west|
F Ratchet
T Goblin Sponsorship (Part 1)
A Goblin Sponsorship (Part 2)
T Wharfmaster Dizzywig |N|On the dock|
A Parts for Kravel
]]

end)

