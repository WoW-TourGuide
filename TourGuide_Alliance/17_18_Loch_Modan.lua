TourGuide:RegisterGuide("Loch Modan (17-18)", "Redridge Mountains (18-20)", "Alliance", function()
return [[
A Filthy Paws |N|Upstairs, inside the tower|
A Stormpike's Order
C Filthy Paws |N|In the mine west of the tower|
T Filthy Paws
R Thelsamar
f Grab flight point
A Ironband's Excavation |N|Downstairs, Last house|
R Ironband's Excavation Site
T Ironband's Excavation
A Gathering Idols
A Excavation Progress Report
C Gathering Idols
T Gathering Idols
R Farstrider Lodge |N|East of Excavation site|
A Crocolisk Hunting
A A Hunter's Boast
C A Hunter's Boast
T A Hunter's Boast
A A Hunter's Challenge
C A Hunter's Challenge
T A Hunter's Challenge
A Vyrin's Revenge (Part 1)
A Bingles' Missing Supplies |N|The NPC can be found at the easternmost shore of The Loch|
C Crocolisk Hunting
C Bingles' Missing Supplies |N|The supplies are scattered around northernmost islands. Blastencapper (54.21,26.61), Hammer (51.78,24.09), Screwdriver (48.37,20.50), Wrench (48.73,30.10)|
R Stonewrought Dam
A A Dark Threat Looms (Part 1) |N|Middle of the dam|
T A Dark Threat Looms (Part 1) |N|Barrel at the east of dam|
A A Dark Threat Looms (Part 2)
T A Dark Threat Looms (Part 2)
R Thelsamar
T Excavation Progress Report
A Report to Ironforge
C Vyrin's Revenge (Part 1) |N|Cave top of the mountain south of Thelsamar|
R Farstrider Lodge
T Crocolisk Hunting
T Vyrin's Revenge (Part 1)
A Vyrin's Revenge (Part 2)
T Vyrin's Revenge (Part 2)
T Bingles' Missing Supplies |N|Remember Bingles at the shore?|
R Ironforge
f Grab flight point
T Report to Ironforge |N|Hall of Explorers. Skip the next quest he tries to give you|
R Stormwind City |N|Use the Deeprun Tram|
T Stormpike's Order
f Grab flight point
R Redridge Mountains
]]
end)