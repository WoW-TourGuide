
TourGuide:RegisterGuide("Eastern Plaguelands (58)", "Western Plaguelands (58-59)", "Alliance", function()
return [[
T Demon Dogs |N|On the west edge of the zone|
T Blood Tinged Skies
T Carrion Grubbage
A Redemption
C Redemption |N|Blah blah blah... he talks to much.|
T Redemption
A Of Forgotten Memories

C Of Forgotten Memories |N|South at the Undercroft (28,86).  Talk to the grave and Mercutio and his adds will walk in (not spawn).  Try to pull him away from the adds, kill and loot him, and get out.  You might need help.|
C Zaeldarr the Outcast |N|(27,85)|
A Hameya's Plea |N|From the scroll near Zaeldarr|

T Of Forgotten Memories |N|Back on the west edge of the zone|
A Of Lost Honor

T Brother Carlin |N|Back at Light�s Hope Chapel|
A Villains of Darrowshire
T Zaeldarr the Outcast |N|Down by the corpse pit|

C Villains of Darrowshire |N|Sword is north of Corin�s Crossing (53,65).  Skull is in Blackwood Lake (51,49)|
C Of Lost Honor |N|Find the flag on the island at TODO Lake (71,33)|

T Troubled Spirits of Kel'Theril |N|At the north tower (56,24).  Skip the follow-up.|
C Defenders of Darrowshire |N|Kill cannibal ghouls, gibbering ghouls and diseased flayers in the northwest part of the zone, talk to the ghosts that spawn.|

T Defenders of Darrowshire |N|Back at Light�s Hope Chapel|
T Villains of Darrowshire
]]
end)

