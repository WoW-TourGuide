

TourGuide:RegisterGuide("Hellfire Peninsula (60-62)", nil, "Alliance", function()
return [[
F Nethergarde Keep
B [Nethergarde Bitter] |N|From Bernie Heisten (63,16)|

A Through the Dark Portal |N|In front of the Dark Portal (58,55)|

R Hellfire Peninsula |N|Through the portal you moron!|
T Through the Dark Portal
A Arrival in Outland
T Arrival in Outland
A Journey to Honor Hold

F Honor Hold
T Journey to Honor Hold
A Force Commander Danath
h Honor Hold
T Force Commander Danath |N|In the keep (56,65)|
A The Legion Reborn
A Know Your Enemy

T Know Your Enemy |N|Follow the road west to the tower (50,60)|
A Fel Orc Scavengers
A Waste not, Want not

T The Legion Reborn |N|Road east of Honor Hold (61,60)
A The Path of Anguish

C Waste not, Want not |N|Directly north, look for seige machines (59,49)|
C Fel Orc Scavengers
C The Path of Anguish |N|Follow the road east (68,56).  Watch out for Fel Reavers!|

T The Path of Anguish
A Expedition Point
T Expedition Point |N|To the east (71,62)
A Disrupt Their Reinforcements
C Disrupt Their Reinforcements |N|Northeast.  Kill till you get 4 runes then blow up Portal Kaalez (72,58).  Do the same for Portal Grimh (71,55)|
T Disrupt Their Reinforcements
A Mission: The Murketh and Shaadraz Gateways
C Mission: The Murketh and Shaadraz Gateways |N|Get a flight from Wing Commander Dabir'ee, blow shit up!|
T Mission: The Murketh and Shaadraz Gateways
A Shatter Point

F Shatter Point |N|Talk to Wing Commander Dabir'ee again|
T Shatter Point
A Wing Commander Gryphongar
T Wing Commander Gryphongar |N|In the half-tower|
A Mission: The Abyssal Shelf
A Go to the Front

C Mission: The Abyssal Shelf |N|Talk to Gryphoneer Windbellow, blow more shit up|
T Mission: The Abyssal Shelf

F Force Camp Front
T Go to the Front |N|Skip "Forge Base Mageddon" unless you want a group|

F Honor Hold |N|Fly back to Shatter Point, then HH... or hearth|
A The Longbeards |N|From the innkeeper|
A An Old Gift
A The Path of Glory |N|Up in the keep|
A Weaken the Ramparts
T Waste Not, Want Not |N|At the west tower|
A Laying Waste to the Unwanted
T Fel Orc Scavengers
A Ill Omens
A Unyielding Souls

A This Caves-a-Rockin |N|At the mine under HH|
A A Job for an Intelligent Man
C This Caves-a-Rockin |N|Inside, of course|
T This Caves-a-Rockin
A The Mastermind
C The Mastermind |N|Inside, at the bottom|
T The Mastermind

]]


--~ 24) Go SW to 45,63 it's a pit, you can see it on your map it looks like a canyon, with some tremor looking
--~ things along the ground, they�re crust busters. When you get close they pop up for you to attack. Kill 15
--~ of them for "A Job for an Intelligent Man" They spawn in and close to this pit. By the time you�re done you
--~ should have an Eroded Leather Pouch drop, if it hasn�t kill until it does. This starts "Missing Missive"
--~ 25) Run down to the zeppelin Crash at 49,74 and accept "Ravager Egg Roundup" and "In Case of
--~ Emergency..." now anywhere between this spot and south of Honor Hold, look for debris you can click on
--~ laying on the ground for this. You need 30 and they�re all over so I can�t list exact coordinates. They look
--~ like the pictures below:
--~ 26) Go down to the Armory at 54,82 and kill the footman, sorcerers, and knights for "Unyielding Souls" You'll
--~ find Mysteries of the Light for "An Old Gift" just outside the busted house that is south of the armory, it's
--~ on the edge of the world here at 54,86.
--~ Once you�ve got that finished, start heading east and look for the zepplin parts for "In Case of Emergency..."
--~ they lie between the zeppelin crash site and Honor Hold. You shouldn�t have a problem getting all 30 is pretty
--~ easy.
--~ 27) Go SE to Zeth'gor at 68,75 and kill the orcs until you get a cursed talisman for "Ill Omens" you don't need
--~ to go into Zeth'gor but kill anything out here until you get one. They fall easiest from grunts, shamans,
--~ and necrolytes
--~ 28) Once you have the talisman, go east to 70,63 at the fallen fel reaver and turn it in to Ironridge accept
--~ "Cursed Talismans"
--~ 29) Hold off on doing this quest for now, we�ll come back here later when we level and make it easy.
--~ 30) Go north of here to the path of glory, which is the road that goes west from the dark portal, be on the
--~ lookout for the bones on the bone road from Hellfire Citadel all the way to the dark portal east for "The
--~ Path of Glory". You need 8 and can find them quickly. Just sweep your mouse until you see a clickable
--~ item. These are extremely easy to overlook. The easiest way to find them is right click and move your
--~ mouse down so your cam is near the ground, then they stick out much easier.
--~ 31) North of the path of glory you will see a line of catapults and orcs. Starting from the east and going west
--~ you'll come across 4 of them that you must destroy for "Laying Waste to the Unwanted" you have to
--~ manually click the torch next to each one to destroy it. The 1st at 58,46 the 2nd at 55,46 the 3rd at 53,47
--~ the 4th at 52,47
--~ 32) Run down the trench beside you, across towards HH, but go east to the tower at 51,60 just outside HH
--~ and turn in "Laying Waste to the Unwanted" then go inside and turn in "Unyielding Souls" accept "Looking
--~ to the Leadership"
--~ 33) Exit the tower and turn right toward the cave and turn in "A Job for an Intelligent Man"
--~ 34) Enter Honor Hold, go in the Inn, then turn in "An Old Gift" then leave the Inn and go up into the castle at
--~ 56,65 and go up top. Turn in "The Path of Glory" accept "The Temple of Telhamat"
--~ 35) You should be about 50% through level 60 or very close to it
--~ 36) Exit HH through the west gate and follow the road NW around the tower to the Temple of Telhamat
--~ 37) The Temple of Telhamat is at 23,40
--~ 38) Accept "Deadly Predators" "Cruel Taskmasters" and "In Search of Sedai" SKIP "The Rock Flayer Matriarch"
--~ 39) Go up the steps and take your first left to the medic and up your first aid past 300. Follow the path north
--~ into the Inn and turn in "The Temple of Telhamat" then accept "The Pools of Aggonar" and then make this
--~ your home.
--~ 40) Go east from the Inn to 25,37 and grab the FP
--~ 41) Go east of the FP to 26,37 and you will see Sedai's Corpse, click it to turn in "In Search of Sedai" accept
--~ "Return to Obadei"
--~ 42) Run back to west to town to 23,40 and turn it in then accept "Makuru's Vengeance"
--~ 43) Go North from where you found Sedai�s Corpse to 29,33, follow the path up to Mag�Har Post and kill the
--~ orcs until you have 10 necklaces for "Makuru's Vengeance"
--~ 44) Go East to the Pools of Aggonar by either jumping down or going to the front at 38,44 and kill Blistering
--~ Rots and Terrorfiends for "The Pools of Aggonar"
--~ 45) Hearth to Telhamat and go to the entrance first then turn in "Makuru's Vengeance" accept "Atonement"
--~ then run up into the Inn, turn in "The Pools of Aggonar" accept "Cleansing the Waters" turn in
--~ "Atonement" accept "Sha'naar Relics"
--~ 46) Run SW to the Cenarion Post at 15,52 and turn in "Missing Missive"
--~ 47) Run south a bit to The Ruins of Sha'naar and kill the taskmasters for "Cruel Taskmasters" They have 2 58
--~ guys with them, but once you kill the taskmaster they become friendly. This can be tough, but if you can
--~ sap or sheep or something to one of the miners it�s a lot easier. Not too hard but it�s close. Also keep an
--~ eye out for the Sha�naar Relics for �Sha�naar Relics�
--~ 48) Go in the SE corner of the camp up the path to a hut at 16,65 and accept "A Traitor Among Us" Now go
--~ back down to the hut at 14,63 and open the chest and grab the key, make sure the 63 elite isn't around
--~ then go back up and turn it in. Accept "The Dreghood Elders" then Go back down and free Morod at 13,60
--~ in the tent, Aylaan in the tent at 13,58 both are on the west side of the ruins. Finally free Akoru at 15,58
--~ in the tent on the east side of the ruins. Go to the tent in the back of the ruins and turn in "The Dreghood
--~ Elders" accept "Arzeth's Demise" Go back down and use the Staff of the Dreghood Elders on the 63 elite to
--~ remove his elite status and kill him, then run back up to tent and turn in "Arzeth's Demise"
--~ 49) Go SE to 23,72 turn in "The Longbeards" accept "The Arakkoa Threat" "Rampaging Ravagers" and
--~ "Gaining Mirren's Trust" You should be friendly now and already have the bitter for it, then turn it back in
--~ and accept "The Finest Down"
--~ 50) Just NW of the longbeards you'll see a big thorny area full of ravagers, kill 10 quillfang ravagers for
--~ "Rampaging Ravagers" then go back to the Longbeard camp at 23,72 and turn it in
--~ 51) Just SE of the longbeard camp is a valley, kill 6 Haal'eshi Talonguards and 4 Windwalkers for "The Arakkoa
--~ Threat" Also look for Kaliri Nests which will spawn a hatchling which drop the feathers for "The Finest
--~ Down" At about 25,76 there is a path above the valley, go up there and kill Avruu and he will drop
--~ �Avruu�s Orb� go farther back, you should see a purple house at 29,81, out front is an orb touch it and a
--~ 63 elemental spawns, you have to fight him to 40% life to free him, I think some classes will have trouble
--~ with this, my rogue did just fine, don�t be afraid to try for a group if you can�t do it alone
--~ 52) Mount and run back up to the Longbeards camp Turn in "The Finest Down" and "The Arakkoa Threat"
--~ 53) Go east of the Haal'eshi valley and you'll see stonescythe whelps and stonescythe alphas. Alphas are more
--~ in higher ground and in the cave at 34,62 for "Deadly Predators"
--~ 54) You're gonna exit the Stonescythe area right into the Southern Rampart at 42,68
--~ 55) Go down SW from here into Razorthorn Trail at 39,86 and start collecting ravager eggs for "Ravager Egg
--~ Roundup"
--~ 56) Go NE to the zeppelin crash site at 49,74 and turn in "Ravager Egg Roundup" accept "Helboar, the Other
--~ White Meat" and turn in "In Case of Emergency..." accept "Voidwalkers Gone Wild"
--~ 57) You should be 61 now or really close to it. If you�re not it�s no biggy, theres nothing for a tiny bit that
--~ needs you to be 61
--~ Hellfire Peninsula 61-62
--~ You will find the deranged helboars all around the zeppelin and to the east of the crash site. The creation
--~ of the purified meat is about 50/50. Then go further south of the zeppelin to 47,80 in the warp fields and
--~ kill the voidwalkers. While killing the voidwalkers, go east to the Expedition Armory to 54,83 and kill
--~ Thalvos and to the north a tiny bit to 53,81 and kill Xintor for "Looking to the Leadership" Real easy ones
--~ to kill. Once you have it all, run back up to the zeppelin and turn in "Helboar, the Other White Meat" SKIP
--~ "Smooth as Butter" it�s just too low of a drop rate, then turn in "Voidwalkers Gone Wild"
--~ 59) Hearth back to Telhamat
--~ 60) Turn in "Sha'naar Relics" accept "The Seer's Relic" and "Helping the Cenarion Post" Turn around and
--~ accept "An Ambitious Plan". Go south of town, turn in "Deadly Predators" and "Cruel Taskmasters"
--~ 61) Go east to 26,37 Sedai's Corpse and use the Seer's Relic on it. Then go to the northern part of the Pools
--~ of Aggonar at 40,31 and you will spawn Aggonar, he's 63, kinda rough but not too hard.
--~ 62) If you�re racing for time, die so you end up at the temple, otherwise run back
--~ 63) Run back up into the Inn, turn in "The Seer's Relic" and "Cleansing the Waters"
--~ 64) Fly to HH
--~ 65) Leave town by the west entrance and head straight for the tower below and turn in "Looking to the
--~ Leadership"
--~ 66) You should be 61 now for sure, you�re probably past 61 a bit at this point
--~ 67) Go back down to the Warp fields around 50,83 and get an uncontrolled voidwalker to about 25% life and
--~ use the crystal on it and grab the red crystal it drops for "An Ambitious Plan" then head to the east
--~ towards Zeth�Gor at 67,75
--~ 68) Go up into Zeth�Gor to the east at 64,74 and kill the Grunts, Necrolytes, and shamans for "Cursed
--~ Talismans" I hit the 25% through 61 mark while doing this. Kinda bad drop rate but a good grind spot
--~ while doing a quest.
--~ 57) Once you�re done turn it back in at Expedition point to the NE at 70,63 accept "Warlord of the Bleeding
--~ Hollow"
--~ 58) Go back up into Zeth'Gor to the back into the big building at 69,76 and go to the center and kill Warlord
--~ Morkh for "Warlord of the Bleeding Hollow" then go back to Expedition Point at 70,63 and turn it in.
--~ 59) Unfortunately that quest chain ends there, but it�s a good xp grind getting the talismans, even though you
--~ lose 250 rep for doing it, at least in beta so it may be a bug.
--~ 60) Now either hearth to Telhamat if your hearth is up, or get on the gryphon to shattered point then fly to
--~ the Temple and run into the Inn, turn in "An Ambitious Plan"
--~ 61) Start using the lfg tool and chat to look for a group for Hellfire Citadel Ramparts for that quest you picked
--~ up earlier. Plus they designed outlands as I explained, to make you do instances, or else you�re gonna hit
--~ 63 � or so and get stuck grinding or instancing them. Try to do HFC before you leave town here for
--~ quests. You could grind instead if you want
--~ 62) Vazruden, the guy who gets off the last dragon, in ramparts will drop a letter called Ominous Letter which
--~ starts "Dark Tidings"
--~ 63) Go back into HH up in the main castle and turn in "Weaken the Ramparts" accept "Heart of Rage" and
--~ "The Blood is Life" then turn in "Dark Tidings" you should now be close to 50% to 62.
--~ 64) Now you can either go and do the Blood Furnace (which is easier than ramparts) or you can finish up the
--~ rest of HFP before you attempt it. I say do it later if you want because running 2 instances can suck, but
--~ as you seen ramparts was pretty short
--~ 65) Enter the wall on the West side of HFC, there are some stairs at 45,58 and follow it up to the entrance
--~ 66) The blood is very easy to get and the investigation is complete when you enter that bottom circle at the
--~ end. Take a look below you too, to see a huge demon for the shattered halls. He�s who keeps talking.
--~ 67) Now leave by the green tunnel and run back to HH, to the big castle up top and turn in "Heart of Rage" to
--~ Danath Trollbane, then turn in "The Blood is Life" to Gunny.
--~ 68) From Telhamat run down SW to Cenarion Post at 15,52 and accept "The Cenarion Expedition" "Keep
--~ Thornpoint Hill Clear!" and turn in "Helping the Cenarion Post" to Thiah Redmane and accept "Demonic
--~ Contamination" SKIP "Colossal Menace" unless you want to do it with a group
--~ 69) Go east of here and start killing Hulking Helboars. There all around HFP but there is a lot to the east. Once
--~ you're done head back West to Cenarion Post and turn it in then accept "Testing the Antidote" then go
--~ back east, find a helboar and use the antidote on him and he�ll turn into dreadtusk, just kill him then go
--~ back and turn it in
--~ 70) Go just west now to Thornpoint Hill and start killing ravagers for "Keep Thornpoint Hill Clear!" make sure
--~ you're at full health before each one. They have some wierd thorn protection and seem to break stuns
--~ early. Go back to Cenarion Point and turn it in
--~ 71) Now overall you should be at least 90% through 61 if you�re not already 62 which you should be after
--~ grinding or doing HFC.
--~ 72) You�ll want to go back to SW to get your new skills. Make sure you go before you head to Zangarmarsh
--~ because there is no flightpath right at the start of this zone. Once you�re done with your skills, hearth to
--~ Telhamat
--~ 73) Run all the way west, through the ravagers, into Zangarmarsh

end)


