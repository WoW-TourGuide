
TourGuide:RegisterGuide("Western Plaguelands (58-59)", nil, "Alliance", function()
return [[
F Western Plaguelands
T A Plague Upon Thee (Part 1)
A A Plague Upon Thee (Part 2)
T Skeletal Fragments

A Unfinished Business (Part 1) |N|(51,28)|

T A Plague Upon Thee (Part 2) |N|(47,32)|
A A Plague Upon Thee (Part 3)
C Unfinished Business (Part 1) |N|(49,42), (51,43)|
T Unfinished Business (Part 1) |N|(51,28)|
A Unfinished Business (Part 2)

C Unfinished Business (Part 2) |N|Huntsman Radly (57,36)|
C The Mark of the Lightbringer |N|Kill Cavalier Durgen at the top of the tower (55,23)|

T Unfinished Business (Part 2) |N|(51,28)|

T Of Lost Honor |N|Back in EPL, by the river (7,43)| |Z|Eastern Plaguelands|
A Of Love and Family (Part 1)
T Of Love and Family (Part 1) |N|In front of Scholomance (65,75).  Skip the follow-up.|

T A Plague Upon Thee (Part 3) |N|Back at Chillwind Camp|
T The Mark of the Lightbringer
A Tomb of the Lightbringer
C Tomb of the Lightbringer |N|Escort quest!|
T Tomb of the Lightbringer |N|Back at Chillwind Camp|

]]

--[[
16) Hearth to IF
17) Go to the throne room at 43,52 and accept �An Earnest Proposition� (pick your class from the list)
You have to have your classes tier 0 bracers in order to do this quest. You can buy it on AH
18) Fly to Menethil and boat to Auberdine, fly to Darnassus
19) Go to 35,8 on the 2nd floor and turn in �Glyphed Oaken Branch�
20) You should be 59, fly to Winterspring
]]

end)

