
TourGuide:RegisterGuide("Zangarmarsh (62-63)", nil, "Alliance", function()
return [[
A The Cenarion Expedition |Z|Hellfire Peninsula| |N|From Amythiel Mistwalker (16,52) in Hellfire, the small Cenarion outpost just south of the main path west.|

R Cenarion Refuge |N|Through Thornfang Ravine. Don't let the local fauna bite!|
A The Umbrafen Tribe
A Plants of Zangarmarsh
A A Warm Welcome |N|From Warden Hamoot on top of the tower.|
A The Dying Balance |N|Inside the inn.|
T The Cenarion Expedition |N| Back of the inn.|
A Disturbance at Umbrafen Lake

N Rep items... |N|Keep any Fertile Spores and Unidentified Plant Parts you find.  Turn the plant parts in to the night elf in the Refuge every chance you get, they only last till honored.  If you get any Uncatalogued Species you should hold on to them until after you reach honored.|
C The Dying Balance |N|Boglash patrols south-west of Cenarion Refuge (82,72).|
C The Umbrafen Tribe |N|Far south of the Refuge toward the road to Terrokar (82,82), Kataru is in the top of the tower (85.28, 90.91).|
A Escape from Umbrafen |N|Escort quest - fairly easy, you get jumped by two packs of two or three nonelites. Starts from Kayra Longmane at the east camp (83.37,85.53).|

T The Umbrafen Tribe |N|Back in Cenarion Refuge|
A A Damp, Dark Place
A Saving the Sporeloks
A Safeguarding the Watchers |N|Right behind you|
T The Dying Balance |N|Inside the inn|
T Escape from Umbrafen

C A Warm Welcome |N|The naga around the east side of the Lagoon (64,64) have claws, unlike most of the other naga in Zangarmarsh.|
C Disturbance at Umbrafen Lake |N|Run by the pump on the southwest side of the lake (70,81).|
C A Damp, Dark Place |N|Funggor Cavern is south of Umbrafen Lake (74,91).  Ikeyen's belongings are on a rock at the back of the cave (70.6, 97.9).|
C Safeguarding the Watchers |N|At the bottom of the cave. It's labeled a group quest, but Klaq is soloable if you clear the adds first.|
C Saving the Sporeloks

T Disturbance at Umbrafen Lake |N|Back at Cenarion Refuge, of course.|
A As the Crow Flies
C As the Crow Flies |N|Use the amulet.| |U|25465|
T As the Crow Flies
A Balance Must Be Preserved
T A Warm Welcome |N|To Warden Hamoot on top of the tower.|
T Safeguarding the Watchers |N|Over the river near the moonwell.|
T A Damp, Dark Place
T Saving the Sporeloks
A Blessings of the Ancients |N|You should be friendly with the Cenarion Expedition by now.|
C Blessings of the Ancients
T Blessings of the Ancients

N Disable Umbrafen Pump |N|Use the seeds at the controls to the Umbrafen Lake pump (70,80)| |U|24355| |Q|Balance Must Be Preserved| |QO|Umbrafen Lake Controls Disabled|
N Disable Lagoon Pump |N|Use the seeds at the controls to the Lagoon pump (64,63)| |U|24355| |Q|Balance Must Be Preserved| |QO|Lagoon Controls Disabled|
R Telredor |N|Northwest of the Refuge, just follow the road. The elevator up is on the east side.|
A The Dead Mire
A The Fate of Tuurem
f Grab flight point |N|Upstairs to your left as you run in. Don't fall over the edge.|
A The Boha'mu Ruins
A The Orebor Haborage
A Unfinished Business
A Fulgor Spores
A Menacing Marshfangs
A Too Many Mouths to Feed
h Telredor

N Shrooms and Rippers... |N|While near Telredor, keep an eye out for Fulgor Spores, bright green mushrooms. Also, kill any Marshfang Rippers you come across.|
C The Dead Mire |N|You're after a pile of dirt in the middle of the Dead Mire (80,43)|
C Unfinished Business |N|Sporewing is along the south edge of the Mire (76,45)|
C Fulgor Spores |N|Bright green mushrooms all around the base of Telredor|
C Menacing Marshfangs

T Fulgor Spores |N|Back up on Telredor|
T Unfinished Business
A Blacksting's Bane
T The Dead Mire
A An Unnatural Drought
T Menacing Marshfangs

A Umbrafen Eel Filets
C Too Many Mouths to Feed |N|Mire Hydras are all around the edge of Umbrafen Lake. Kill any Eels you see too.|
C Umbrafen Eel Filets |N|Umbrafen Lake has eels? Who'da thunk it?|

A What's Wrong at Cenarion Thicket? |N|From Lethyn Moonfire in Cenarion Refuge.|
A Watcher Leesa'oh
C An Unnatural Drought |N|Kill Withered Giants in the Dead Mire.|
A Withered Basidium |U|24483| |N|Kill Withered Giants until you get the item to start this|
N Save Bog Lord Tendrils |N|You'll need them later for reputation.|

T An Unnatural Drought |N|Back at Telredor|
T Withered Basidium
A Withered Flesh
T Umbrafen Eel Filets
T Too Many Mouths to Feed
A Diaphanous Wings

N Glowcaps in your ass |N|Loot any Glowcaps you find, you'll need them for quests and reputation rewards.|
N Disable Serpent Lake Pump |N|Use the seeds at the controls to the Serpent Lake pump (61,40).| |U|24355| |Q|Balance Must Be Preserved| |QO|Serpent Lake Controls Disabled|
C Blacksting's Bane |N|Blacksting is just north of Feralfen Village (49,59).|
C The Boha'mu Ruins |N|Boha'mu is south of Feralfen Village (45,68).|

K "Count" Ungula |N|To the west, typically found south of Zabra'Jin (33,59).| |L|25459|
A The Count of the Marshes |U|25459|
T Watcher Leesa'oh |N|The Cenarion Watchpost is on the road south toward Nagrand. (23,66)|
T The Count of the Marshes
A Observing the Sporelings

A The Sporeling's Plight |N|From Fahssn, west of the watchpost (19,64).|
A Natural Enemies
T Natural Enemies
C Observing the Sporelings |N|You need to go a bit deeper into the Spawning Glen for this (13,62).|
C The Sporeling's Plight |N|All over the Spwaning Glen.|
T The Sporeling's Plight
A Sporeggar
T Sporeggar |N|Just north-east of the Spawning Glen (19,51)|
A Glowcap Mushrooms
T Glowcap Mushrooms
A Fertile Spores
T Fortile Spores
N Friendly - Sporeggar |N|Complete every repeatable you can until you're friendly.  You can turn in Glowcaps, Bog Lord Tendrils and Mature Spore sacs.  All these repeats end once you're friendly, so use them while you can!|
A Now That We're Friends...

T Observing the Sporelings |N|At the watchpost.|
A A Question of Gluttony
C A Question of Gluttony |N|East of the watchpost, look for green glowing things on the ground.|
T A Question of Gluttony
A Familiar Fungi
N Disable Marshlight Lake Pump |N|Use the seeds at the controls to the Marshlight Lake pump (25,42).| |U|24355| |Q|Balance Must Be Preserved| |QO|Marshlight Lake Controls Disabled|
C Now That We're Friends... |N|These naga are all around the west side of Marshlight Lake.|
T Now That We're Friends...

R Orebor Harborage |N|Northwest of Serpent Lake.|
f Grab the flight point

A Secrets of the Daggerfen
T The Orebor Harborage
A Ango'Rosh Encroachment
A Daggerfen Deviance
A Wanted: Chieftain Mummaki |N|Off a signpost outside the inn.|
C Ango'Rosh Encroachment |N|The Hewn Bog is just southwest of Orebor.|
C Familiar Fungi
C Wanted: Chieftain Mummaki |N|In the tower (24,27)|
C Secrets of the Daggerfen |N|The manual is at the top of the tower (24,27). The vial is a random spawn at the three camps (look on your map).|
C Daggerfen Deviance

T Secrets of the Daggerfen |N|Back at Orebor Harborage|
T Ango'Rosh Encroachment
A Overlord Gorefist
T Daggerfen Deviance
T Wanted: Chieftain Mummaki

h Orebor Harborage
A Natural Armor |N|From the armor merchant, Maktu.|
A Stinger Venom |N|From Puluu in the hut near the front of town.|
A Lines of Communication
A The Terror of Marshlight Lake
C Drain Schematics |O| |N|You need to go to the centre of Serpent Lake.|
C Natural Armor |N|Fenclaw Thrashers are around the pipes in the center of Serpent Lake.|
C Lines of Communication |N|Marshfang Slicers are all around Zabra'Jin, the Horde town southwest of Serpent Lake. Don't tangle with the guards.|
T Familiar Fungi |N|At the watchpost, west of the Spawning Glen (23,66).|
A Stealing Back the Mushrooms
C Stinger Venom |N|Every side of Marshlight Lake except the east side.|
C The Terror of Marshlight Lake |N|Terrorclaw is on the southern-most island in Marshlight Lake, west and a bit south of the pump (22,45). He also fears, so be aware.|
C Overlord Gorefist |N|Far northwest of the zone, the little island with a bridge to it (18,9).|
C Stealing Back the Mushrooms

H Orebor Harborage
T Overlord Gorefist
T Natural Armor
A Maktu's Revenge
T Stinger Venom
T Lines of Communication
T The Terror of Marshlight Lake
C Maktu's Revenge |N|Mragesh is on the westernmost island in Serpent Lake. (41,41)|
T Stealing Back the Mushrooms |N|Getting tired of running to the watchpost yet?|
N Underbog |N|If you're planning to do Underbog, now would be a good time to grab the three quests in Sporeggar.|
T Maktu's Revenge

F Telredor
T The Boha'mu Ruins
A Idols of the Feralfen
T Blacksting's Bane
T Diaphanous Wings
h Telredor
C Idols of the Feralfen |N|On the ground at Feralfen Village (46,60).|

T Balance Must Be Preserved |N|Back at Cenarion Refuge|
A Drain Schematics |U|24330| |N|You should have gotten an item from the naga.| |O|
N The drain chain... |N|This is an optional chain, but you have the item so you should do it.  You just have to run out to the Cenarion outpost in Hellfire and back.  Free EXP and a little jogging for your chubby ass.  The follow-up will send you into Coilfang instances.|
T Drain Schematics |O|
C Withered Flesh |N|In the Dead Mire, north of the Refuge|

T Withered Flesh |N|Up on Telredor|
T Idols of the Feralfen
A Gathering the Reagents
C Gathering the Reagents |N|You want to kill normal Spore Bats, not the greater or lesser.|
T Gathering the Reagents
A Messenger to the Feralfen
C Messenger to the Feralfen |N|Feralfen Village is in the southern hills of Zangarmarsh (43,69).|
T Messenger to the Feralfen
]]

end)