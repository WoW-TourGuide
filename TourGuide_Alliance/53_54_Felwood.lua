
TourGuide:RegisterGuide("Felwood (53-54)", "Un'goro Crater (54)", "Alliance", function()
return [[
F Astranaar
h Astranaar

R Felwood |N|Head east out of town.  At the fork near the retreat head north out of the zone (55,31)| |Z|Ashenzale|
A Cleansing Felwood |N|On the north side of the road (54,86)|

R Emerald Sanctuary |N|Just ahead, north of the road (51,81)|
f Get flight point
A Forces of Jaedenar
A The Corruption of the Jadefire
A Verifying the Corruption

A Timbermaw Ally |N|Down by the road (51,85)|
C Timbermaw Ally |N|Just to the southwest|
T Timbermaw Ally
A Speak to Nafian

U Open your ooze containers |U|11912|
K Kill Cursed Oozes |N| Follow the road north to the Ruins of Constellas (40,69).  Use the vials after you kill and loot.| |U|11914| |Q|A Little Slime Goes a Long Way| |QO|Filled Cursed Ooze Jar: 6/6|
C The Corruption of the Jadefire |N|To the west, Xavathras can be found at the far west end of the ruins (32,67)|
C A Little Slime Goes a Long Way (Part 1) |N|At the next set of ponds north along the road (40,59)| |U|11948|
C Forces of Jaedenar |N|To the west, outside the caves (37,59)|

T Forces of Jaedenar
A Collection of the Corrupt Water
T The Corruption of the Jadefire
A Further Corruption
C Collection of the Corrupt Water |N|Fill the vial at the corrupted moonwell in Jaedenar (51,82)| |U|12922|

T Collection of the Corrupt Water
A Seeking Spiritual Aid

C Verifying the Corruption |N|North at Shatter Scar Vale (40.52, 41.78).  Watch out for the elite infernals!|
K Kill Xavaric |L|11668| |N|North in Jadefire Run (39,22)|
A Flute of Xavaric |N|From the item he dropped, naturally| |U|11668|
C Further Corruption
C Flute of Xavaric
C Cleansing Felwood |N|Kill elementals at Irontree Cavern (55,17)|

R Talonbranch Glade |N|At the northeast edge of the zone, south of the road (62,24)|
f Grab the flight point
T Speak to Nafian |N|North, at the end of the road (64.8,8.2)|
A Deadwood of the North
C Deadwood of the North
T Deadwood of the North
A Speak to Salfa

R Winterspring |N|Thru the cave!|
T Speak to Salfa
A Winterfall Activity
T The New Springs |N|To the south near the hot springs (31.27, 45.20)| |Z|Winterspring|
A Strange Sources
T It's a Secret to Everybody (Part 3)
A The Videre Elixir (Part 1)
A Threat of the Winterfall

R Moonglade |N|Back thru the tunnel, go north at the middle|
f Grab flight point |N|Follow the road east a bit (48,67)| |Z|Moonglade|

F Emerald Sanctuary
T Further Corruption
T Flute of Xavaric
A Felbound Ancients
T Verifying the Corruption
T Cleansing Felwood |N|South along the road|

F Feralas
A Jonespyre's Request
T Jonespyre's Request
A The Mystery of Morrowgrain
T The Mystery of Morrowgrain

N Stable your pet |C|Hunter|
N Tame an Ironfur Patriarch |C|Hunter| |N|On the mainland, 48+.  You're after Claw (Rank 7)|

C The Videre Elixir (Part 1) |N|Buy some "bait" from Gregan Brewspewer at the Twin Colossals (45.1, 25.5).  Take the road north to Ruins of Ravenwind, find Miblon Snartooth (44.6, 10.2), drop the bait, and grab the Evoroot.| |Z|Feralas| |U|11141|
T The Videre Elixir (Part 1) |N|Back at Gregan.|

H Astranaar
N Get your pet back |C|Hunter|
F Talonbranch Glade
t Train |C|Druid|
t Train |C|Hunter|

R Winterspring
T The Videre Elixir (Part 2)
A Meet at the Grave

R Everlook |N|Follow the road east.|
F Ratchet |N|Flight point is northeast outside the town wall (62.32, 36.61)| |Z|Winterspring|
T Volcanic Activity
T Seeking Spiritual Aid |N|To the south at the Tidus Stair (65.80, 43.76)| |Z|The Barrens|
A Cleansed Water Returns to Felwood
]]
end)

