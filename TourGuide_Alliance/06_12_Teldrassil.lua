TourGuide:RegisterGuide("Teldrassil (6-12)", "Darkshore (12-17)", "Alliance", function()
return [[
A Zenn's Bidding |N|Along the road to Dolannar (60,56)|
R Dolanaar
h Dolanaar
T Dolanaar Delivery
T Crown of the Earth (Part 2)
A Crown of the Earth (Part 3)
A Denalan's Earth |N|At the tower|
A A Troubling Breeze |N|In the tower|
t First Aid |N|In the tower|
A The Emerald Dreamcatcher |N|In the tower|
A Twisted Hatred |N|In the tower|

C Zenn's Bidding |N|East of Dolanaar|
C Crown of the Earth (Part 3) |N|Moonwell is East of Dolanaar (63.36,59.09)|
T A Troubling Breeze |N|East in Starbreeze Village(66,58)|
A Gnarlpine Corruption
C The Emerald Dreamcatcher |N|East of the questgiver in a house is a Dresser (68.01,59.66)|
T Zenn's Bidding
A Seek Redemption! |N|At the tower at Dolanaar|
N Keep an eye out for Fel Cones |N|Needed for "Seek Redemption!".  Found at the bases of trees|

T Denalan's Earth |N|South at the lake (60,68)|
A Timberling Seeds
A Timberling Sprouts
C Timberling Seeds
C Timberling Sprouts
T Timberling Seeds
T Timberling Sprouts
A Rellian Greenspyre

R Dolanaar
T Crown of the Earth (Part 3)
A Crown of the Earth (Part 4)
T Gnarlpine Corruption |N|In the tower|
A The Relics of Wakening
T The Emerald Dreamcatcher
A Ferocitas the Dream Eater |N|

C Seek Redemption!
T Seek Redemption!
C Ferocitas the Dream Eater |N|North of Starbreeze (68,53)|

R Dolanaar |N|Or die and use spirit ressurrection|
C Twisted Hatred |N|North of town (54,52).  You can also wait until you're 10 to do this.|
T Ferocitas the Dream Eater
T Twisted Hatred

A The Road to Darnassus |N|From a mounted patrol on the road west of town|
C The Road to Darnassus |N|Near the cave south of the road where the NPC partrols (46,52)|
R Ban'ethil Barrow Den |N|West of Dolanaar (44,57)|
N Keep an eye out for relics |N|Four items needed for "The Relics of Wakening" are in this cave|
A The Sleeping Druid |N|In the cave at (44,57), questgiver at (45,61)|
C The Sleeping Druid |N|Kill shamans|
T The Sleeping Druid
A Druid of the Claw
C Druid of the Claw
C The Relics of Wakening
T Druid of the Claw

C Crown of the Earth (Part 4) |N|South around (42,67)|

R Dolanaar |N|Or die and spirit rez|
T Crown of the Earth (Part 4)
A Crown of the Earth (Part 5)
T The Relics of Wakening
A Ursal the Mauler
T The Road to Darnassus

A Taming the Beast (Part 1) |C|Hunter|
C Taming the Beast (Part 1) |C|Hunter| |N|Tame a webwood lurker (58,60)|
T Taming the Beast (Part 1) |C|Hunter|
A Taming the Beast (Part 2) |C|Hunter|
C Taming the Beast (Part 2) |C|Hunter| |N|Tame a nightsaber stalker (55,73)|
T Taming the Beast (Part 2) |C|Hunter|
A Taming the Beast (Part 3) |C|Hunter|
C Taming the Beast (Part 3) |C|Hunter| |N|Tame a strigid screecher (55,73)|
T Taming the Beast (Part 3) |C|Hunter|
N Tame a Strigid Hunter |C|Hunter| |N|To get Claw (Rank 2) and Growl (Rank 2)|
N Tame your pet |C|Hunter| |N|Elder Nightsaber (42,42) is nice|

A Heeding the Call |C|Druid|

R Darnassus

T Rellian Greenspyre
A Tumors 

T Training the Beast |C|Hunter| |N|(40,8)|

T Heeding the Call |C|Druid| |N|Nortwest in Darnassus, Cenarion Enclave (33,8)|
A Moonglade |C|Druid|
T Moonglade |N| Teleport there, go into the house past the light armor salesmen| |C|Druid| 
A Great Bear Spirit |C|Druid|
T Great Bear Spirit |C|Druid| |N|Northwest corner of Moonglade (39,28)|
A Back to Darnassus |C|Druid|
T Back to Darnassus |C|Druid| |N|Hearthstone back to Teldrassil, return to the guy that sent you to Moonglade|
A Body and Heart |C|Druid| 
R Rut'theran Village |N|The portal west of the bank| |C|Druid|
F Auberdine |C|Druid|
C Body and Heart |N|The cave west of Auberdine. You might want to ask someone to help you to complete this! (43.50,45.97)| |C|Druid|
F Darnassus |C|Druid|
T Body and Heart |C|Druid|

A Nessa Shadowsong |N|On your way out of Darnassus|

A The Glowing Fruit |N|Out of town, to the south (42,76)|
C Ursal the Mauler |N|West of the glowing tree|

C Tumors |N|Elementals at the river east of The Oracle Glade (42,42)|
C Crown of the Earth (Part 5) |N|Moonwell at The Oracle Glade|
A The Enchanted Glade
C The Enchanted Glade |N|Harpys southwest of The Oracle Glade|
T The Enchanted Glade
A Teldrassil

R Darnassus
T Tumors
A Return to Denalan
T Teldrassil |N|Atop the tower|
A Grove of the Ancients

H Dolanaar
T Crown of the Earth (Part 5)
A Crown of the Earth (Part 6)
T Ursal the Mauler

T Return to Denalan |N|South at the lake|
A Oakenscowl
T The Glowing Fruit

C Oakenscowl |N|Go along the river to the west until you find the cave|
T Oakenscowl

R Darnassus
T Crown of the Earth (Part 6) |N|Turn it in at the boss of Darnassus, Atop of the tower|

R Rut'theran Village |N|Run thru the portal west of the bank|
T Nessa Shadowsong
A The Bounty of Teldrassil
T The Bounty of Teldrassil |N|At the hippogryph master|
A Flight to Auberdine
F Auberdine
]]
end)

