
TourGuide:RegisterGuide("Netherstorm (67-70)", nil, "Alliance", function()
return [[
R Netherstorm |N|From Evergrove take the path east up top, then go north and follow the path|
A Off To Area 52 |N|On Gyro-Plank Bridge|

R Area 52 |N|Back down the road (32,62)|
f Grab the flight point |N|To the left outside the city|
A Securing the Shaleskin Shale |N|You may have to wait for him to respawn|
T Off To Area 52
A You're Hired!
h Area 52
T Report to Spymaster Thalodien |N|Scryers| |O|
A Manaforge B'naar |N|Scryers|
T Assist Exarch Orelis |N|Aldor| |O|
A Distraction at Manaforge B'naar |N|Aldor|
A The Archmage's Staff

R Ruins of Enkaar |N|To the north (31,56), follow the road then take the north fork|
A Recharging the Batteries
A Mark V is Alive!
C Mark V is Alive!
C You're Hired!
C Recharging the Batteries |N|To the north.  Take phase hunters down to about 25% life, use the battery recharging blaster on them, then kill them|
T Mark V is Alive!
T Recharging the Batteries
C Securing the Shaleskin Shale

R Area 52
T Securing the Shaleskin Shale
A That Little Extra Kick
T You're Hired!
A Invaluable Asset Zapping
A Report to Engineering
T Report to Engineering |N|To the south|
A Essence for the Engines

C Essence for the Engines |N|North, near the glowing trenches|
C That Little Extra Kick |N|Around the rocks, near the shaleskin flayers|

R Area 52
T That Little Extra Kick
T Essence for the Engines
A Elemental Power Extraction

C Manaforge B'naar |N|Scryers, West out of town, then southwest| |O|
C Distraction at Manaforge B'naar |N|Aldor, West out of town, then southwest| |O|
C Elemental Power Extraction |N|South at the Crumbling Waste (32,78).  Use the power extractor on the Sundered rumblers and warp aberrations then kill them.|
C Invaluable Asset Zapping |N|Northeast at the Arklon Ruins (41,74), (41,72), (40,73), (42,72)|
C The Archmage's Staff |N|Use the conjuring powder on the fountain in the center of the ruins|

R Area 52
T Invaluable Asset Zapping
A Dr. Boom!
T Elemental Power Extraction
T The Archmage's Staff
A Rebuilding the Staff
T Distraction at Manaforge B'naar |O|
T Manaforge B'naar |O|
A Measuring Warp Energies
A Assisting the Consortium |N|Aldor|
A Naaru Technology |N|Aldor|
T Assisting the Consortium |O|
A Consortium Crystal Collection

C Dr. Boom! |N|Northeast of town at Camp of Boom (34,60).  Run in, bomb him, and run out.  Repeat till dead.| |U|29429|
C Rebuilding the Staff |N|Back north at Ruins of Enkaat|

R Area 52
T Rebuilding the Staff
A Curse of the Violet Tower
T Dr. Boom!
N You should be roughly 75% to 68

C Measuring Warp Energies |N|Use the orb where the pipes go into the ground in this order: North (25,59), West (20,67), South (20,71), East (28,72)| |U|29324|
T Naaru Technology |N|At the console inside the building| |O|
A B'naar Console Transcription |N|Aldor|
C Consortium Crystal Collection |N|Kill Pentatharon at Arklon Ruins (41,73)|
A Needs More Cowbell |N|From Thadell in Town Square (57,85)|
A Indispensable Tools
A Malevolent Remants |N|By the tower|
T Curse of the Violet Tower |N|Use Archmage Vargoth's Staff at the top of the tower|
A The Sigil of Krasus
A The Unending Invasion |N|Back down the tower|
A A Fate Worse Than Death

N Kill Abjurist Belmara |N|South of the tower|
A Abjurist Belmara |N|From the tome she dropped|
N Kill Battle-Mage Dathric |N|Inside the town hall building (60,87)|
A Battle-Mage Dathric |N|Again, from the dropped item|
C Indispensable Tools |N|Kill the apprentices at the blacksmith house (60,85)|
N Kill Conjurer Luminrath
A Conjurer Luminrath |N|Yet another item-started quest|
N Kill Cohlien Frostweaver |N|Little gnome, what do we do with gnomes? Oh yes, we KILL THEM!|
A Cohlien Frostweaver |N|Do these item-started quests ever end?|

N Clear the path over the bridge to...
T Needs More Cowbell
A When the Cows Come Home
C When the Cows Come Home |N|Stay back and let Bessy get hit first each time, so that she stops and gives you time to kill things|
T When the Cows Come Home
T Indispensable Tools
A Master Smith Rhonsus

C Battle-Mage Dathric |N|To the west, First house on the left (56,86).  Use the weapon rack inside|
C Conjurer Luminrath |N|Two houses up, a broken house (56,87), use the dresser|
C Cohlien Frostweaver |N|Two more houses up (55,87). Use the footlocker|
C The Unending Invasion |N|Fragment is right in front of the house (56,87)|
C Abjurist Belmara |N|Use the bookshelf in the next house (55,86)|
C A Fate Worse Than Death
C Malevolent Remnants

T The Unending Invasion |N|Back up by the tower|
A Potential Energy Source
T A Fate Worse Than Death
T Malevolent Remnants
A The Annals of Kirin'Var
T Battle-Mage Dathric
T Conjurer Luminrath
T Cohlien Frostweaver
T Abjurist Belmara

C The Annals of Kirin'Var |N|Kill Battle-Mage Dathric again in the town hall (60,87)|
C Master Smith Rhonsus |N|Kill Rhonsus at the blacksmith house (60,87)|

T The Annals of Kirin'Var |N|Back at the tower|
A Searching for Evidence
T Master Smith Rhonsus |N|At the barn|
N You should be 68 now or very close
T Searching for Evidence |N|At the necromantic focus at Bessy's barn (60,78)|
A A Lingering Suspicion
C A Lingering Suspicion |N|Kill ghosts in the chapel yard|
T A Lingering Suspicion
A Capturing the Phylactery
C Capturing the Phylactery |N|Across from the barn at Chapel Yard, the middle outhouse|
T Capturing the Phylactery
N Skip "Destroy Naberious!" |N|Unless you want a group|

C Potential Energy Source |N|West at Manaforge Coruu (51,83)|
C The Sigil of Krasus |N|Spellbinder Maryana, wandering around outside|
T The Sigil of Krasus |N|Use the archmage's staff|
A Krasus's Compendium

T Potential Energy Source |N|Back at the tower|
A Building a Perimeter
C Krasus's Compendium |N|Chapter 3 in the house south of the tower (58,87), Chapter 1 in the house on the other side (58,89), Chapter 2 in the house 2 doors down (57,89)|
T Krasus's Compendium |N|Use the staff again|
A Unlocking the Compendium

C Building a Perimeter |N|Use the rune activator at the 3 runes: East (59,85), Northwest (57,82) and West (54,86)|
T Building a Perimeter |N|Back at the tower|
A Torching Sunfury Hold
A The Sunfury Garrison

C Torching Sunfury Hold |N|North at Sunfury Hold (56,81).  Kill flamekeepers to get a torch, then torch tests and ballistas.  You can torch the same tent and ballista many times, they respawn fast.|
C The Sunfury Garrison
C Unlocking the Compendium |N|Kill Spellweaver Marathelle around the lowest platform (56,78).|
T Unlocking the Compendium |N|Use the staff, you should know this by now|
A Summoner Kanthin's Prize

C Summoner Kanthin's Prize |N|Over the bridge to the north at Manaforge Duro (59,62).  Look for the water elemental|
T Summoner Kanthin's Prize
A Ar'kelos the Guardian

T Torching Sunfury Hold |N|Back at the town square|
T The Sunfury Garrison
A Down With Daellis

C Ar'kelos the Guardian |N|At the tower entrance|
T Ar'kelos the Guardian |N|Top of the tower|
N Skip Finding the Keymaster |N|Unless you get a group|

C Down With Daellis |N|On the path west of Manaforge Duro (57,65)|
T Down With Daellis |N|Back at the tower|

R Cosmowrench |N|East of Manaforge Duro (65,66)|
f Grab flight point
A Bloody Imp-ossible!
C Bloody Imp-ossible! |N|Summon the imp and kill warp chasers.  You must be in melee range when they die.| |U|31815|
T Bloody Imp-ossible!

F Area 52
A Pick Your Part
T Consortium Crystal Collection
A A Heap of Ethereals
T Measuring Warp Energies
T B'naar Console Transcription |O|
A Shutting Down Manaforge B'naar

F Shattrath City
t Train (Level 68) |N|Port to a city, of course|

H Area 52
N You should now be about 45-50% to 69

C Shutting Down Manaforge B'naar |N|Kill Overseer Theredis inside the manaforge, talk to the console|
C Pick Your Part |N|At The Heap (27,77)|
C A Heap of Ethereals

T Pick Your Part |N|Back at Area 52|
A In A Scrap With The Legion
A Help Mama Wheeler
T A Heap of Ethereals
A Warp-Raider Nesaad
T Shutting Down Manaforge B'naar
A Shutting Down Manaforge Coruu
A Attack on Manaforge Coruu

C Warp-Raider Nesaad |N|Back at The Heap (28,80)|
C Shutting Down Manaforge Coruu |N|East over the bridge (46,81).  Kill Overseer Seylanna (49,81) and use the console, like before.|
C Attack on Manaforge Coruu

A Drijya Needs Your Help |N|Northwest inside the dome (46,56)|
T Help Mama Wheeler
A One Demon's Trash...
A Run a Diagnostic!
A New Opportunities
A Keeping Up Appearances
T Drijya Needs Your Help |N|To the south, skip his escort quest unless you have help|

C One Demon's Trash... |N|North at The Scrap Field (50,59)|
C In A Scrap With The Legion
C Run a Diagnostic! |N|A bit further north (48,55)|
C Keeping Up Appearances |N|Run around in the dome killing lynx|
C New Opportunities

T One Demon's Trash...
A Declawing Doomclaw
T New Opportunities
T Run a Diagnostic!
A Deal With the Saboteurs
T Keeping Up Appearances
A The Dynamic Duo

R Area 52
T In A Scrap With The Legion
T Warp-Raider Nesaad
A Request for Assistance
T Shutting Down Manaforge Coruu
A Shutting Down Manaforge Duro
T Attack on Manaforge Coruu
A Sunfury Briefings

R Midrealm Post
T Request for Assistance
A Rightful Repossession

C Declawing Doomclaw |N|East from the dome (50,57)|
C Shutting Down Manaforge Duro |N|East at Manaforge Duro.  You know the drill, kill Overseer Athanel (60,68)|
C Rightful Repossession
C Sunfury Briefings

T Rightful Repossession
A An Audience with the Prince
T Declawing Doomclaw
A Warn Area 52!

R Area 52
T Warn Area 52!
A Doctor Vomisa, Ph.T.
T Shutting Down Manaforge Duro |N|Skip "Shutting Down Manaforge Ara"|
T Sunfury Briefings
A Outside Assistance
T Doctor Vomisa, Ph.T. |N|East out of town at a little tent (37,63).  Skip You, Robot|

C Deal With the Saboteurs |N|Crocs at the lake in the Eco-Dome (46,53)|
A To the Stormspire |N|Back at Midrealm Post|
N You should be ~80% to 69

R The Stormspire |N|Follow the road out to the bigger dome (41,32)|
A Flora of the Eco-Domes
T Deal With the Saboteurs
T To the Stormspire
A Diagnosis: Critical
A Surveying the Ruins
A The Minions of Culuthas
A Fel Reavers, No Thanks!
f Grab flight point
T An Audience with the Prince
A Triangulation Point One
T The Dynamic Duo
A Retrieving the Goods

h Stormspire

C Flora of the Eco-Domes |U|29818| |N|Use the modulator on the lashers, then kill them|
C Diagnosis: Critical |N|At the generator to the northeast (47,27)| |U|29803|
C Surveying the Ruins |N|At the ruins northeast, just outside the dome.  Markers are at (51,20), (54,22), (55,19)| |U|29445|
C The Minions of Culuthas
C Fel Reavers, No Thanks! |N|West at Forge Camp: Oblivion (37,28), kill Mekgineers for 5 gas, use on one of the reavers (35,28), (36,25), (37,25). You only need to do this on ONE reaver!|

T Outside Assistance |N|Tuluman's Landing (34,38)|
A A Dark Pact
A Dealing with the Foreman
A Neutralizing the Nethermancers

C Neutralizing the Nethermancers |N|At Manaforge Ara|
C A Dark Pact |N|Gan'arg Warp-Tinkerers and Daughters of Destiny near Manaforge Ara|
T Dealing with the Foreman |N|In the Trelleum Mine (26,42)|
A Dealing with the Overmaster
C Dealing with the Overmaster |N|Down the tunnel, right then left|

H Stormspire
T Diagnosis: Critical
A Testing the Prototype
T Surveying the Ruins
T The Minions of Culuthas
T Fel Reavers, Not Thanks!
A The Best Defense
T Flora of the Eco-Domes |N|Bottom of the elevator|
A Creatures of the Eco-Domes

C Creatures of the Eco-Domes |N|Fight down to 20% and tag em| |U|29817|
T Creatures of the Eco-Domes
A When Nature Goes Too Far
C When Nature Goes Too Far |N|At the lake to the northeast (44,28)|
T When Nature Goes Too Far
T A Dark Pact |N|At Tuluman's Landing (34,38)|
A Aldor No More
T Dealing with the Overmaster
T Neutralizing the Nethermancers

C The Best Defense |N|Northwest at Forge Base: Gehenna (39,21)|
T Testing the Prototype |N|Northeast at Eco-Dome Farfield (44,14)|
A All Clear!
C All Clear!
C Retrieving the Goods
T All Clear!
A Success!
T Success! |N|Back up on the Starmspire|
T The Best Defense
A Teleport This!
T Retrieving the Goods

C Teleport This! |N|Back at Forge Base: Gehenna, teleporters at (39,20), (41,18), (44,20)|
T Teleport This!

F Area 52
T Aldor No More
H Stormspire
A A Not-So-Modest Proposal |N|At Protectorate Watch Post (58,31)|
A Electro-Shock Goodness!
A The Ethereum
A Recipe for Destruction
A Captain Tyralius


]]

--[[
|U|29737|
140) Drink Navuud�s Concoction and hit the void wastes to split them into globules for the first part of
"Electro-Shock Goodness!" down in the staging grounds where we�re about to go
141) Go just south of here into the Ethereum Staging Grounds at 56,38 and start killing for "The
Ethereum" Captain Zovax wanders around. Once you're done you have to use the transponder zeta at
56,38 and Ameer will appear. Turn it in and accept "Ethereum Data" then to the south a tiny bit at 55,39
is the data cell, grab it and go turn it in to Ameer at the portal and accept "Potential For Brain Damage=
High"
142) Now you have to kill the guys for their essence. Drink the essence and you have a 30 second buff to
see the fish like relays floating around that you have to kill for the relay data. You should be able to get 1-
2 datas per essence you drink
143) Turn it back in to Ameer at the portal and accept "S-A-B-O-T-A-G-E" then kill an archon or overlord
until you get a prepared ethereum wrapping then go back to the teleporter and turn it in and accept
"Delivering the Message" and protect the demolitionist (easiest way is to either clear it first, or let stuff
attack him. It does that thing where it chases him but doesn�t hit him) then go back to the teleporter and
turn it in and SKIP the next part
144) Just a bit SW there is warden Icoshock, kill him for the key but don't use it on any of the prisons
around him. They're duds. The real prison is just behind the Nexus-King Salhadaar at 53,41 open it for
"Captain Tyralius"
145) Go east to 59,45 and speak with Araxes and accept "The Flesh Lies..." then enter the mine, Access
Shaft Zeon and use the lighter on the withered corpses. Don't get too close or they spawn into parasitic
fleshbeasts. As you enter the tunnel you can go left, keep left until you end up at 60,41 and you'll see Ya
six and the power pack, accept "Arconus the Insatiable" and he�ll give you his hologram to help then grab
the power pack for "A Not-So-Modest Proposal" then go back up towards the entrance and go deeper in,
staying right until you hit the big room at 60,39 and kill Arconus the Insatiable then leave the cave and
return to Araxes just outside at 59,45 and turn in "The Flesh Lies..."
146) Go up around Manaforge Ultris and drink Navuud's Concoction and hit the seeping sludges to split
them into globules for the other part of "Electro-Shock Goodness!" also kill the unstable voidwraiths and
the voidshriekers here for "Recipe for Destruction"
147) Go to 66,33 and you should get the complete message for "Triangulation Point One" if not use the
triangualtion device and a hunters mark will appear where you have to stand
148) Go east into the Celestial Ridge and down the long path through the nether drakes and at the bottom
at 70,39 you'll see the teleporter right as the ramp down ends. Use it and Marid will appear, turn in "A
Not-So-Modest Proposal" accept "Getting Down to Business"
149) Now go around kill any of the nether drakes or dragons for their essence for this then turn it in at the
Shrouded Figure just behind the teleporter at 70,38 and accept "Formal Introductions" then go a tiny bit
north to 71,35 to Tyri and turn it in and accept "A Promising Start"
150) Now go around the ridge collecting nether dragonkin eggs laying around the crystals then go back to
Tyri and turn it in and accept "Troublesome Distractions"
151) Ride back west to Protectorate Watch Post at 58,31 and talk to Dabiri next to the nether drake and
turn in "Recipe for Destruction" and accept "On Nethery Wings" then find Viridius walking around and turn
in "Captain Tyralius"
152) Go to Navuud and turn in "Electro-Shock Goodness!" then to Ameer and turn in "Arconus the
Insatiable"
153) Go to Hazzin and turn in "Triangulation Point One" accept "Triangulation Point Two" then go to Marid
and talk to him to lure him away and then kill him for "Troublesome Distractions". He has 2 68 guards but
they drop easy
154) Talk to the nether drake now and he'll fly you up top of Ultris, as it spirals you up, keep tossing the
mana bombs until the void conduit is destroyed. Pretty easy to kill if you spam the bombs. When you land
turn in "On Nethery Wings" SKIP "Dimensius the All-Devouring"
155) Go east down into the Celestial Ridge again to Tyri at 71,35 and turn in "Troublesome Distractions"
SKIP "Securing the Celestial Ridge"
156) Hearth to Stormspire then go SW into Manaforge Ara to 28,41 and you should get complete for
"Triangulation Point Two" this spot is near the forge and around mobs so you probably have to run in and
over it. Again I'm not sure if you need to have the mark on it to get the point but I think you do
157) Now go to Tuluman at Tuluman's Landing at 34,37 and turn it in and SKIP "Full Triangle"
158) Now head back to Stormspire
159) Now we go one of 2 places. If you want the skills you can train at 69 go to Shattrath and make it your
home then portal to a town and hearth back. If not, or after getting skills, fly to Allerian Stronghold and
east to the bridge at 71,50 and enter Shadowmoon Valley
--]]

end)

