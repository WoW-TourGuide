
TourGuide:RegisterGuide("Stranglethorn Vale (38-40)", "Badlands (40-41)", "Alliance", function()
return [[
b Booty Bay
A The Bloodsail Buccaneers (Part 1) |N|(28,76)|
A Scaring Shaky |N|In the building next to the upside down half boat|
h Booty Bay
A Venture Company Mining |N|Near the innkeeper (27,77)|
T The Rumormonger |N|Upstairs|
A Dream Dust in the Swamp
A Skullsplitter Tusks
T Sunken Treasure (Part 4) |N|Skip the next part|

C Raptor Mastery (Part 3) |N|Around (33,39)|
C Some Assembly Required |N|Kill Snapjaw Crocolisks from the stream (38,30) to the lake (41,19)|
C Water Elementals |N|On the island (20,23)|

G Grind to level 39 |N|These elementals or raptors and basalisks around (33,39)|
G Grind east, past the road |N|To (41,41)|
C Venture Company Mining
C Skullsplitter Tusks |N|In the camps around (42,37)|
C Panther Mastery (Part 4) |N|Bhag'thera is anywhere between just north of Mosh�ogg (49,25) to just north of the ZG entrance (48,17)|

H Booty Bay
T Venture Company Mining
T Skullsplitter Tusks |N|Upstairs|
T Water Elementals |N|Skip next part|
T Some Assembly Required
A Excelsior

T The Bloodsail Buccaneers (Part 1) |N|Run out of BB, then West.  Letter on a barrel in the pirate camp (27,69)|
A The Bloodsail Buccaneers (Part 2)
C Scaring Shaky |N|Gorillas just northeast of BB (32,65)|

R Booty Bay
T Scaring Shaky
A Return to MacKinley
T The Bloodsail Buccaneers (Part 2) |N|Along the lower dock|
A The Bloodsail Buccaneers (Part 3)
T Return to MacKinley |N|In the house across from the half ship|
A Voodoo Dues
A Up to Snuff |N|Upstairs in the inn|
T The Bloodsail Buccaneers (Part 3)
A The Bloodsail Buccaneers (Part 4)
N Buy pages from the AH |N|Buy any pages you don't have.  You need these pages: 1, 4, 6, 8, 10, 11, 14, 16, 18, 20, 21, 24, 25, 26, 27|

C Excelsior |N|Elite crocolisk near Grom'Gol.  Stear clear of the base if you don't like dying to guards|
R Nesingwary's Expedition |N|(35,10)|
T Raptor Mastery (Part 3)
T Panther Mastery (Part 4)
A Raptor Mastery (Part 4)
A The Green Hills of Stranglethorn
T The Green Hills of Stranglethorn
G Grind to level 40 |N|You should have less than 25% to go|

H Booty Bay
T Excelsior
F Ironforge

t Train (Level 40)
B Mount
]]

end)

