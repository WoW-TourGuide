TourGuide:RegisterGuide("Lakeshire (27-28)", "Duskwood (28-29)", "Alliance", function()
return [[
A Blackrock Bounty
A Blackrock Menace
A Solomon's Law |N|Townhall|
A Wanted: Lieutenant Fangore |N|Poster infront of inn|
h Lakeshire

A An Unwelcome Guest |N|West of lakeshire|
C An Unwelcome Guest
T An Unwelcome Guest

C Blackrock Menace |N|Follow the road north to Render's Camp|
C Blackrock Bounty
A Missing In Action |N|Escort in the cave (28.39,12.56)| |NODEBUG|
C Missing In Action |NODEBUG|

T Missing In Action |NODEBUG|
T Blackrock Menace
T Blackrock Bounty

C Solomon's Law
C Wanted: Lieutenant Fangore

H Lakeshire Inn
T Solomon's Law
T Wanted: Lieutenant Fangore
]]
end)