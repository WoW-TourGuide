TourGuide:RegisterGuide("Wetlands (25-27)", "Lakeshire (27-28)", "Alliance", function()
return [[
A Claws from the Deep
A Young Crocolisk Skins
A War Banners |N|Up the castle|
A Digging Through the Ooze
A The Third Fleet
A The Greenwarden
h Deepwater Tavern
T The Absent Minded Prospector (Part 4)
A The Absent Minded Prospector (Part 5)
B [Flagon of Mead]
T The Third Fleet
A The Cursed Crew
A In Search of The Excavation Team (Part 1) |NODEBUG|
C Young Crocolisk Skins
C Claws from the Deep
R Whelgar's Excavation Site
N Run up the ramp and grab the fossil thats inside the cave |L|5234 1|
T In Search of The Excavation Team (Part 1) |NODEBUG|
A In Search of The Excavation Team (Part 2) |NODEBUG|
A Uncovering the Past
A Ormer's Revenge (Part 1)
C Ormer's Revenge (Part 1)
C The Absent Minded Prospector (Part 5)
T Ormer's Revenge (Part 1)
A Ormer's Revenge (Part 2)
C Ormer's Revenge (Part 2)
C Uncovering the Past
T Ormer's Revenge (Part 2)
A Ormer's Revenge (Part 3)
T Uncovering the Past
C Ormer's Revenge (Part 3)
T Ormer's Revenge (Part 3)
R Angerfang Encampment
C War Banners
A Daily Delivery |N|Crossroad north from Angerfang Encampment|
T The Greenwarden |N|East of the crossroad, across the small river|
A Tramping Paws
C Tramping Paws |N|Camps are located south of Greenwarden|
T Tramping Paws
A Fire Taboo
C Fire Taboo |N|Camps start from north of Greenwarden and continue to the west over the road|
T Fire Taboo
A Blisters on The Land |N|Do this while you do other quests. The questmobs are stealthed and can't be found unless you go near them| |NODEBUG|
H Deepwater Tavern

T The Absent Minded Prospector (Part 5) |N|Inn, upstairs|
T War Banners |N|Castle|
A Nek'rosh's Gambit
T Daily Delivery
T Young Crocolisk Skins
A Apprentice's Duties
T Claws from the Deep
A Reclaiming Goods
T In Search of The Excavation Team (Part 2) |NODEBUG|
F Menethil Harbor
A Fall of Dun Modr |N|NPC on his back infront of castle|
T Reclaiming Goods |N|Damaged Crate at the murloc camp north of Menethil Harbor (13.51,41.37)|
A The Search Continues
T The Search Continues |N|Sealed Barrel at the next murloc camp to the north (13.60,38.22)|
A Search More Hovels
T Search More Hovels |N|Half-buried Barrel at the next murloc camp to the north (13.93,34.81)|
A Return the Statuette
C The Cursed Crew |N|Ships north from the murloc camps|
C Apprentice's Duties
R Ironbeard's Tomb
C Digging Through the Ooze
T Fall of Dun Modr |N|North of the zone|
C Blisters on The Land |NODEBUG|
T Blisters on The Land |NODEBUG|
H Deepwater Tavern
T The Cursed Crew
A Lifting the Curse
T Digging Through the Ooze
T Apprentice's Duties
T Return the Statuette
F Ironforge
T An Old Colleague
F Stormwind City
T A Scroll from Mauren |N|The Mage Quarter|
F Redridge Mountains

]]
end)