TourGuide:RegisterGuide("Darkshore (23-24)", "Ashenvale (24-25)", "Alliance", function()
return [[
h Auberdine
T WANTED: Murkdeep!
T The Absent Minded Prospector (Part 2)
A The Absent Minded Prospector (Part 3)
A A Lost Master (Part 1)|N|In the last house|
R Darnassus
T The Absent Minded Prospector (Part 3) |N|On the west side of Temple of the Moon, outside|
A The Absent Minded Prospector (Part 4)
R Ruins of Mathystra
C Mathystra Relics
A Gyromast's Retrieval |N|Standing on a stone at the beach north of ruins|
C Gyromast's Retrieval |N|Reef Crawlers drop the bottom, Forestriders drop the top and Murlocs drop the middle|
C A Lost Master (Part 1) |N|Northeast corner of the zone|
T Gyromast's Retrieval
A Gyromast's Revenge
C Gyromast's Revenge |N|Use key in the First Mate south on the beach. He will attack you later. Kill him|
T Gyromast's Revenge
R Auberdine
T A Lost Master (Part 1)
A A Lost Master (Part 2)
R Wildbend River
T A Lost Master (Part 2)
A Escape Through Force
C Escape Through Force
H Auberdine
T Escape Through Force
A Trek to Ashenvale
F Astranaar

]]
end)