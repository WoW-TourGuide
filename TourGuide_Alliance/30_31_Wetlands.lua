TourGuide:RegisterGuide("Wetlands (30-31)", "Hillsbrad Foothills (31-32)", "Alliance", function()
return [[
b Menethil Harbor |N|Fly to Auberdine then take the boat.|
C Lifting the Curse |N|Kill the Captain for the key, walk up the rudder to reach him (14,24)|
T Lifting the Curse |N|Open the box on bottom floor of the boat, it's underwater.|
A The Eye of Paleth
T Nek'rosh's Gambit |N|Click on the catapault to finish, ignore next quest unless you want to kill the elite (47.5,46.9)|

R Dun Modr |N|North at top of map (51,21)|
A The Thandol Span (Part 1)
T The Thandol Span (Part 1) |N|Go down the stairs to the right. There are two elites, but they can be soloed one by one (51.2,8)|
A The Thandol Span (Part 2)
T The Thandol Span (Part 2)
A The Thandol Span (Part 3)

R Arathi Highlands |N|Go north.|
C The Thandol Span (Part 3) |Z|Arathi Highlands| |N|Across the little bridge (48,88)|
T The Thandol Span (Part 3)
A Plea To The Alliance
]]
end)