TourGuide:RegisterGuide("Darkshore (20-21)", "Ashenvale (21-22)", "Alliance", function()
return [[
A Onward to Ashenvale |N|Northernmost building in Auberdine|
R Grove of the Ancients |N|Get back to Auberdine and run south|
T Onu
A The Master's Glaive
R The Master's Glaive |N|Southwest from Grove of the Ancients|
C The Master's Glaive
T The Master's Glaive |N|Use the [Phial of Scrying] and click the Scrying Bowl|
A The Twilight Camp
T The Twilight Camp |N|Click the Twilight Tome in the middle of island|
A Return to Onu
A Therylune's Escape |N|Escort really near the Twilight Tome, southeast from it at the same island|
C Therylune's Escape
R Remtravel's Excavation |N|West from The Twilight Camp|
T The Absent Minded Prospector (Part 1)
A The Absent Minded Prospector (Part 2) |N|Escort around the excavation|
C The Absent Minded Prospector (Part 2)
C WANTED: Murkdeep! |N|Clear the camp, after that there's 3 waves of murlocs. Murkdeep is in the last wave|
R Grove of the Ancients
T Return to Onu
A Mathystra Relics
A The Sleeper Has Awakened |N|Escort. REMEMBER TO TAKE THE HORN FROM THE BOX NEXT OF HIM!|
C The Sleeper Has Awakened |N|Use the [Horn of Awakening] when he falls asleep. That happens quite alot...| |U|13536|
]]
end)