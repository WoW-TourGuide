TourGuide:RegisterGuide("Duskwood (28-29)", "Ashenvale (29-30)", "Alliance", function()
return [[
R Darkshire
f Grab flight point |N|East of Darkshire, above the hill|

A Look To The Stars (Part 1) |N|Southeast from flight point in the house| |NODEBUG|
B 1x [Bronze Tube] |N|The Engineer vendor sells this sometimes| |L|4371 1|
T Look To The Stars (Part 1) |NODEBUG|
A Look To The Stars (Part 2) |NODEBUG|
A Worgen in the Woods (Part 1)
A Raven Hill
A The Hermit
A Deliveries to Sven
A The Legend of Stalvan (Part 1)
A The Totem of Infliction
h Darkshire
A The Night Watch (Part 1)
T The Legend of Stalvan (Part 1)
C Worgen in the Woods (Part 1) |N|West of Darkshire (64,46)|

T Worgen in the Woods (Part 1) |N|Back in Darkshire|
A Worgen in the Woods (Part 2)

C Worgen in the Woods (Part 2) |N|West of Darkshire (64,46)|
T Worgen in the Woods (Part 2)
A Worgen in the Woods (Part 3)
T Look To The Stars (Part 2) |N|(81,59)| |NODEBUG|
A Look To The Stars (Part 3) |NODEBUG|
C The Night Watch (Part 1)
K Skeletons for 10x Skeleton Fingers |L|2378 10|
C Look To The Stars (Part 3) |N|Insane Ghoul inside the chapel| |NODEBUG|
A An Old History Book |O| |U|2794|
C Worgen in the Woods (Part 3) |N|(73,73)|

H Darkshire |N|Or run since it is not far.|
T The Night Watch (Part 1)
A The Night Watch (Part 2)
T Worgen in the Woods (Part 3)
A Worgen in the Woods (Part 4)
T Worgen in the Woods (Part 4)
T Look To The Stars (Part 3) |N|Southeast of Darkshire, in a shack| |NODEBUG|
A Look To The Stars (Part 4) |NODEBUG|

C Look To The Stars (Part 4) |N|At Vol'Gol Ogre Mound, in a cave (34,77)| |NODEBUG|
T Raven Hill |N|In Raven Hill Cemetery|
C The Night Watch (Part 2)
T The Hermit |N|Shack north of the cemetery|
A Supplies from Darkshire
A The Weathered Grave |N|Grave at (17,29)|
T Deliveries to Sven |N|At The Hushed Bank (8,34)|
A Sven's Revenge
H Darkshire

T The Night Watch (Part 2)
A The Night Watch (Part 3)
T The Weathered Grave
A Morgan Ladimore
T Morgan Ladimore
T Supplies from Darkshire
A Ghost Hair Thread
T Look To The Stars (Part 4) |NODEBUG|
T Ghost Hair Thread |N|Southeast of Darkshire, in a shack|
A Return the Comb
T Return the Comb
A Deliver the Thread

T Sven's Revenge |N|Find the dirt mound at The Yorgen Farmstead (49,77)|
A Sven's Camp

T Deliver the Thread |N|Shack north of cemetary.|
A Zombie Juice
C The Night Watch (Part 3)

K Undeads for 10x [Ghoul Fang] |L|1129 10|
K Spiders for 5x [Vial of Spider Venom] |L|1130 5|

C The Totem of Infliction
T Sven's Camp
A The Shadowy Figure

H Scarlet Raven Tavern
T Zombie Juice
T The Night Watch (Part 3)
T The Totem of Infliction
T The Shadowy Figure
A The Shadowy Search Continues
T The Shadowy Search Continues
A Inquire at the Inn
T Inquire at the Inn

A An Old History Book |U|2794| |O|
T An Old History Book |N|Turn in to the Stormwind Library.| |O|
]]
end)