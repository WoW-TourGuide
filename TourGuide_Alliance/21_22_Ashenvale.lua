TourGuide:RegisterGuide("Ashenvale (21-22)", "Stonetalon Mountains (22-23)", "Alliance", function()
return [[
T The Sleeper Has Awaken |N|You need to talk to the NPC in Maestra's Post before quest completes|
T The Tower of Althalaxx (Part 3)
A The Tower of Althalaxx (Part 4)
A Bathran's Hair
C The Tower of Althalaxx (Part 4) |N|Northeast of Maestra's Post|
C Bathran's Hair |N|Plant bundles at (31,21)
T The Tower of Althalaxx (Part 4)
A The Tower of Althalaxx (Part 5)
T Bathran's Hair
A Orendil's Cure
T Therylune's Escape |N|Southwest from Maestra's Post, across the road to the ruins|
R Astranaar
f Grab the flight point |N|Northwest entrance of Astranaar|
A The Zoram Strand
A On Guard in Stonetalon (Part 1)
A Journey to Stonetalon Peak
T Onward to Ashenvale |N|Inside the Inn|
A Raene's Cleansing (Part 1)
A Culling the Threat
h Astranaar
N Stable your pet |C|Hunter|
T Orendil's Cure
A Elune's Tear
R The Zoram Strand
A The Ancient Statuette |N|NPC at the small camp south of The Zoram Strand|
N Tame Clattering Crawler |C|Hunter| |N|Make sure it's lvl20 so you'll get Claw Rank 3. Teach Growl Rank 3|
C The Zoram Strand
C The Ancient Statuette |N|On the ground next the the ruins visible on map (14.20,20.64)|
T The Ancient Statuette
A Ruuzel
C Ruuzel |N|At the island northwest of the zone. No need to kill Ruuzel if you find the Rare mob called 'Lady Vespia'. She drops the same item|
T Ruuzel
T Raene's Cleansing (Part 1) |N|The small puddle southeast of The Zoram Strand|
A Raene's Cleansing (Part 2)
C Raene's Cleansing (Part 2) |N|Kill the murlocs to get the item|
H Astranaar
T The Zoram Strand
A Pridewings of Stonetalon
T Raene's Cleansing (Part 2)
A Raene's Cleansing (Part 3)
A An Aggressive Defense
N Unstable your Cat |C|Hunter|
C Elune's Tear |N|At the puddle northeast of Astranaar|
T Raene's Cleansing (Part 3) |N|East of the puddle|
A Raene's Cleansing (Part 4)
R Silverwind Refuge |N|South, a house next to the lake. Grind your way unless you are level 22|
t Train level 22 |C|Hunter|
C An Aggressive Defense |N|Camp northeast of Silverwind Refuge|
H Astranaar |N|Or just run there if heartstone is on cooldown|
T An Aggressive Defense
T Elune's Tear |N|East of Inn|
A The Ruins of Stardust
R The Ruins of Stardust |N|South of Astranaar|
C The Ruins of Stardust |N|Grab Stardust covered bushes|
R The Talondeep Path

]]

end)