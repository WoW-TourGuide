TourGuide:RegisterGuide("Dun Morogh (Dwarf)", "Dun Morogh (3-11)", "Alliance", function()
return [[
A Dwarven Outfitters
C Dwarven Outfitters
T Dwarven Outfitters

A Encrypted Rune |C|Rogue|
A Etched Rune |C|Hunter|
A Hallowed Rune |C|Priest|
A Simple Rune |C|Warrior|
A Consecrated Rune |C|Paladin|

A Coldridge Valley Mail Delivery (Part 1)
A A New Threat
C A New Threat
T A New Threat

T Tainted Memorandum |C|Warlock|
T Glyphic Memorandum |C|Mage|
T Encrypted Memorandum |C|Rogue|
T Simple Memorandum |C|Warrior|
]]
end)