TourGuide:RegisterGuide("Stonetalon Mountains (22-23)", "Darkshore (23-24)", "Alliance", function()
return [[
A Super Reaper 6000 |N|In a hut southwest of Windshear Crag (58.99,62.60)|
T On Guard in Stonetalon (Part 1) |N|On the hill south from the hut|
A On Guard in Stonetalon (Part 2)
T On Guard in Stonetalon (Part 2)
A A Gnome's Respite
C Super Reaper 6000 |N|Venture Co. Operators drop the blueprints|
C A Gnome's Respite
T Super Reaper 6000
T A Gnome's Respite |N|(59,66)|
A An Old Colleague
A A Scroll from Mauren
R Mirkfallon Lake
C Pridewings of Stonetalon
T Journey to Stonetalon Peak
F Auberdine
]]
end)