
TourGuide:RegisterGuide("Stranglethorn Vale (41-42)", nil, "Alliance", function()
return [[
F Stormwind City
A In Search of The Temple |N|In the Dwarven District (63,24)| |Z|Stormwind City|

F Booty Bay
h Booty bay
A Goblin Sponsorship |N|From Baron Revilgaz in the Inn (Top Floor)|
A Skullsplitter Tusks |N|From Kebok in the inn (2nd Floor).|
A Tran'Rek |N|From Krazek in the Inn (2nd Floor)|
A Zanzil's Secret |N|From Crank Fizzlebub in the Inn.|
A Akiris by the Bundle (Part 1) |N|From Privateer Bloads next to the Bank.|
A The Bloodsail Buccaneers (Part 1) |N|From First Mate Crazz just past the "Old Port Authority" building (First big building when getting off the Boat).|

C "Pretty Boy" Duncan |N|Outside, head west right after exiting Booty Bay.  He's on the shore (28,69)|
T The Bloodsail Buccaneers (Part 1) |N|On top of one of the barrels|
A The Bloodsail Buccaneers (Part 2)
C Akiris by the Bundle (Part 1) |N|Northwest across the bridge (24,65)|
C Scaring Shaky |N|Head back to the Booty Bay entrance, then follow the path north a bit (31,67)|

T Scaring Shaky |N|At "Shaky" Philippe on the peer|
A Return to MacKinley
T The Bloodsail Buccaneers (Part 2) |N|At First Mate Crazz a bit further towards the Inn.|
A The Bloodsail Buccaneers (Part 3)
T Akiris by the Bundle (Part 1) |N|At Privateer Bloads next to the bank.|
A Akiris by the Bundle (Part 2)
T "Pretty Boy" Duncan |N|At Catelyn the Blade inside the Inn.|
A The Curse of the Tides
A Up to Snuff |N|From Deeg on the 2nd floor of the Inn.|
T The Bloodsail Buccaneers (Part 3) |N|At Fleet Master Seahorn on the 2nd Floor of the Inn.|
A The Bloodsail Buccaneers (Part 4)
T Return to MacKinley |N|At "Sea Wolf" MacKinley in the house right next to the Inn.|
A Voodoo Dues
A Keep an Eye Out |N|From Dizzy One-Eye near the Booty Bay blacksmith (Left when entering Booty Bay from the tunnel).|

C The Bloodsail Buccaneers (Part 4) |N|From the Booty Bay exit head east to the coast, then south.  Items can be found at the two camps (30,81) (27,83), and in a rowboat (28,83).|
C Keep an Eye Out |N|Kill Bloodsails|
C Up to Snuff |N| Kill Bloodsails|

C Venture Company Mining |N|From the Booty Bay exit take the road north beyond Gurubashi Arena (39,41).  Kill Venture Co. Goblins|
C Raptor Mastery (Part 3) |N|To the northwest across the road (34,37).|
C Panther Mastery (Part 3) |N|Shadowmaw Panther are stealth, but you can find a lot to the northeast (36,35).|

C Skullsplitter Tusks |N|To the east at Ziata'Jai Ruins (42,36).  Clear that camp, then hit Balia'mah Ruins (45,33) to the northeast and then Zul'Mamwe (47,38) to the southeast.  Repeat!|
G Level 42 |N|Keep looping through the troll ruins|

C Goblin Sponsorship (Part 4) |O| |N|Follow the road north to the Venture Co. Base at Lake Nezferiti, go to the oil rig (42,18). Go on top of it, kill Foreman Cozzle, loot his key. Jump down and go to the small house next to the lumber mill, open the chest and get the Fuel Regulator Blueprints.|
C Kurzen's Mystery |N|First scroll is in a tablet named "Moon Over the Vale" in the Bal'lal Ruins (29,20).  Second is in a tablet named "Gri"lek the Wanderer", west of Bal'lal Ruins underwater (24,22).  Third and Fourth are in the Ruins of Zul'Kunda (21,10).  Third is in "Fall of Gurubashi" (23,12).  Fourth is in "The Emperor's Tomb" (25,9)|

T Panther Mastery (Part 3) |N|Back at Nessingwary's Camp (35,10)|
T Raptor Mastery (Part 3)
T Kurzen's Mystery |N|North at the rebel camp|

H Booty Bay
T Venture Company Mining |N|At Crank Fizzlebub in the Inn.|
T Up to Snuff |N|At Deeg on the 2nd Floor of the Inn.|
T Skullsplitter Tusks |N|At Kebok, 2nd floor of the Inn.|
T Goblin Sponsorship (Part 4) |N|At Baron Revilgaz, 2nd Floor of the Inn.|
A Goblin Sponsorship (Part 5)
T The Bloodsail Buccaneers |N|At Fleet Master Seahorn, 2nd Floor of the Inn.|
T Keep an Eye Out |N|At Dizzy One-Eye to the right of the Blacksmith.|

C Voodoo Dues |N|Run north to Ruins of Jubuwal (33,51), kill Jon-Jon the Crow and Maury "Club Foot" Wilkins.  Run south a tad (33,53) and then east to Ruins of Aboraz (40,57), kill Chucky "Ten Thumbs"|
C Zanzil's Secret |N|Kill the trolls, but avoid Zanzil|

R Booty Bay
T Zanzil's Secret |N|At Crank Fizzlebub in the inn.|
T Voodoo Dues |N|At "Sea Wolf" MacKinley (First house next to the Inn).|
A Cracking Maury's Foot
A Stoley's Debt

]]

end)

