
TourGuide:RegisterGuide("Badlands (40-41)", nil, "Alliance", function()
return [[
B [Frost Oil] |N|From AH|
B [Gyrochronatom] |N|Don't buy if you didn't find a Frost Oil|
B [Healing Potion] |N|Don't buy if you didn't find a Frost Oil and Gyrochronatom|
B [Lesser Invisibility Potion] |N|Don't buy if you didn't find a Frost Oil and Gyrochronatom|
A Ironband Wants You! |N|From Prospector Stormpike in the Hall of Explorers (74,12)| |Z|City of Ironforge|

N Upgrade your ammo! |C|Hunter|

F Loch Modan
N Stable your pet |C|Hunter|
h Thelsamar
A Badlands Reagent Run |N|In the house behind the Inn (37,49)| |Z|Loch Modan|
T Ironband Wants You! |N|Ironband's Excavation Site (65,65)| |Z|Loch Modan|
A Find Agmond

R Badlands |N|(46,76)|
A Fiery Blaze Enchantments |N|(54,43)|
A Mirages
A A Dwarf and His Tools
T Martek the Exiled |N|(42,52)|
A Indurium
A Barbecued Buzzard Wings
A Pearl Diving

N Kill any buzzards you see |N|For "Barbecued Buzzard Wings" and "Badlands Reagent Run"|
N Tame a Ridge Stalker Patriarch |N|(20,53) For Dash and Prowl (Rank 2)| |C|Hunter|
H Thelsamar |C|Hunter|
N Switch back to your cat |N|Don't dismiss the Badlands cat!| |C|Hunter|
R Badlands |C|Hunter|

C Mirages |N|Grind your way to the crate in the northwest at Camp Kosh (66,21)|
A A Sign of Hope (Part 1) |N|Grind your way to the dig site (53,29).  Note is on the right side of the dig (53,33)|
C A Dwarf and His Tools |N|Kill the dwarfs|

T Mirages |N|Grind your way back (53,43)|
A Scrounging
T A Dwarf and His Tools
T A Sign of Hope (Part 1)
A A Sign of Hope (Part 2)

A Tremors of the Earth (Part 1) |N|From a blood elf to the southeast (61,54).  Grind along the way.|
N Keep an eye out for the ogre pack |N|They spawn to the south of the questgiver (62,70).  They patrol down to the southwest side of the zone and back in a circle.  You should be able to pick Boss Tho'grun off the back of the pack easily|

T Find Agmond |N|To the south to the Ogre camp (62,70), clear it, then grind to the dead dorf (50,62)|
A Murdaloc
C Murdaloc |N|To the south in the trogg camp|
C Indurium
C Barbecued Buzzard Wings

T Barbecued Buzzard Wings |N|Back at the neutral camp in the center of the zone (42,52)|
T Indurium
A News for Fizzle

A Study of the Elements: Rock (Part 1) |N|From Lotwil Veriatus to the west (24,44)|
A Coolant Heads Prevail |L|3829| |O|
T Coolant Heads Prevail |O|
A Gyro... What? |L|4389| |O|
T Gyro... What? |O|
A Liquid Stone |L|3823| |O|
T Liquid Stone |O|

C Study of the Elements: Rock (Part 1) |N|Kill Lesser Rock Elementals to the west near Kargath (18,41)|
T Study of the Elements: Rock (Part 1) |N|(26,44)|
A Study of the Elements: Rock (Part 2)
C Study of the Elements: Rock (Part 2) |N|Rock Elementals in the same area as the last step|
T Study of the Elements: Rock (Part 2)
A Study of the Elements: Rock (Part 3)

C Badlands Reagent Run |N|Kill buzzards (15,60) and coyotes (33,62)|
C Scrounging |N|Kill ogres in the southwest (11,77)|
C Study of the Elements: Rock (Part 3) |N|Greater Rock Elements (14,88)|

T Study of the Elements: Rock (Part 3)
A This Is Going to Be Hard (Part 1)
T This Is Going to Be Hard (Part 1)
A This Is Going to Be Hard (Part 2)
T This Is Going to Be Hard (Part 2)
A This Is Going to Be Hard (Part 3)
C This Is Going to Be Hard (Part 3) |N|Kill the summoned elemental!|
T This Is Going to Be Hard (Part 3)

T Scrounging |N|(53,43)|
T Tremors of the Earth (Part 1) |N|(61,54) Skip part 2|
G Level 41 |N|Kill whelps in the ravine to the east|
T Fiery Blaze Enchantment |O|

H Thelsamar
T Badlands Reagent Run
A Badlands Reagent Run II
T Murdaloc |N|Ironband's Excavation Site (65,65)| |Z|Loch Modan|
A Agmond's Fate

]]
end)

