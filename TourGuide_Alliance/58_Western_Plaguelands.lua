
TourGuide:RegisterGuide("Western Plaguelands (58)", "Eastern Plaguelands (58)", "Alliance", function()
return [[
F Western Plaguelands
T The Eastern Plagues
A The Blightcaller Cometh
T The Blightcaller Cometh |N|In Stormwind Keep.  Skip the follow-up.  You can turn this in the next time you're in town, you have no other reason to go to Stormwind right now.|

T Auntie Marlene |N|House in Sorrow Hill (49,78)|
A A Strange Historian
C A Strange Historian |N|Gravestone just north of the house (49,76)|
T A Strange Historian |N|In the inn in Andorhal (39.43, 66.81)|
A The Annals of Darrowshire
A A Matter of Time

C A Matter of Time |N|Find the blue glowy silos around the edges of Andorhal (45.05, 62.73).  Use horn.|  |U|12627|
C The Annals of Darrowshire |N|In the town hall (43.91, 69.22).  Loot books till you find it.|

T A Matter of Time |N|Back at the inn|
A Counting Out Time
T The Annals of Darrowshire
A Brother Carlin

C Counting Out Time |N|Find lunchboxes in the houses all around Andorhal.|
C Skeletal Fragments |N|Kill undead all over Andorhal.|

T Counting Out Time |N|Back at the inn|
]]

end)

