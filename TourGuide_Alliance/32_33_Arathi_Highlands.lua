TourGuide:RegisterGuide("Arathi Highlands (32-33)", "Desolace (33-35)", "Alliance", function()
return [[
F Refuge Pointe
A Worth Its Weight in Gold
A Northfold Manor
C Northfold Manor |N|Go Northwest.|
T Northfold Manor
T Hints of a New Plague? |N|West of Refuge Pointe (60.1,53.8)|
C Worth Its Weight in Gold |N|Go Southeast.|
A MacKreel's Moonshine |N|Jump from bridge to the platform (43.2,92.6)|

H Southshore
T MacKreel's Moonshine

F Refuge Pointe
T Worth Its Weight in Gold
]]
end)