
TourGuide:RegisterGuide("Shadowglen (1-6)", "Teldrassil (6-12)", "Alliance", function()
return [[
A The Balance of Nature (Part 1)
C The Balance of Nature (Part 1)
T The Balance of Nature (Part 1)
A The Balance of Nature (Part 2)

A Encrypted Sigil |C|Rogue|
A Etched Sigil |C|Hunter|
A Hallowed Sigil |C|Priest|
A Simple Sigil |C|Warrior|
A Verdant Sigil |C|Druid|

A The Woodland Protector (Part 1)
A A Good Friend
T The Woodland Protector (Part 1) |N|Dryad to the southwest (57.77, 45.20)| |Z|Teldrassil|
A The Woodland Protector (Part 2)
C The Woodland Protector (Part 2)
T The Woodland Protector (Part 2)
A Webwood Venom |N|At the base of the ramp (57.79, 41.70)| |Z|Teldrassil|

T Encrypted Sigil |C|Rogue|
T Etched Sigil |C|Hunter| |N|Up the ramp!|
T Hallowed Sigil |C|Priest|
T Simple Sigil |C|Warrior|
T Verdant Sigil |C|Druid| |N|Up the ramp!|

t Train (Level 2) |N|Notice! There are no reminders to train levels after this. Remember to do so after even levels|

C The Balance of Nature (Part 2) |N|Mobs are to the northeast (60,35)| |Z|Teldrassil|
C Webwood Venom |N|To the northwest at Shadowthread Cave (57,33)| |Z|Teldrassil|
T A Good Friend |N|Just west of the cave entrance|
A A Friend in Need
T Webwood Venom
A Webwood Egg

T The Balance of Nature (Part 2)
T A Friend in Need
A Iverron's Antidote (Part 1)

C Iverron's Antidote (Part 1) |N|Lilies around the pond, mushrooms at the bases of trees, ichor from the spiders|
C Webwood Egg |N|Inside the cave (56,26)| |Z|Teldrassil|
T Webwood Egg
A Tenaron's Summons
T Tenaron's Summons |N|Up the ramp|
A Crown of the Earth (Part 1)
T Iverron's Antidote (Part 1)
A Iverron's Antidote (Part 2)
C Crown of the Earth (Part 1) |N|Moonwell north of Aldrassil (59.93, 33.07)| |Z|Teldrassil| |U|5185|
T Iverron's Antidote (Part 2)
T Crown of the Earth (Part 1)
A Crown of the Earth (Part 2)

A Dolanaar Delivery |N|South out of Shadowglen (61,47)| |Z|Teldrassil|
A Zenn's Bidding |N|Along the road to Dolannar (60,56)| |Z|Teldrassil|
]]
end)

