TourGuide:RegisterGuide("Hillsbrad Foothills (31-32)", "Arathi Highlands (32-33)", "Alliance", function()
return [[
R Refuge Pointe |N|In Arathi Highlands (44.5,49.3)| |Z|Arathi Highlands|
f Grab flight point!
T Plea To The Alliance

R Southshore |N|In Hillsbrad Foothills (50.8,51.2)|
f Grab flight point!
h Southshore
T Southshore
A Missing Crystals
A Down the Coast
A Soothing Turtle Bisque
A Hints of a New Plague?
A Bartolo's Yeti Fur Cloak
A Costly Menace
A Syndicate Assassins

C Down the Coast
T Down the Coast
A Farren's Proof (Part 1)
C Farren's Proof (Part 1)
T Farren's Proof (Part 1)
A Farren's Proof (Part 2)
T Farren's Proof (Part 2)
A Farren's Proof (Part 3)
T Farren's Proof (Part 3)
A Stormwind Ho!
C Stormwind Ho!
T Stormwind Ho!
A Reassignment

K Kill turtles |N|All along the creek.  Need [Turtle Meat] x10 for "Soothing Turtle Bisque"| |L|3712 10|
C Missing Crystals |N|(55.4,35)|

R Alterac Mountains |N|Go north (54,9)|
C Syndicate Assassins |N|(58,67)| |Z|Alterac Mountains|
A Encrypted Letter |N|Examine documents on the table in the camp.|
A Foreboding Plans |N|Examine documents on the table in the camp.|
C Costly Menace |N|Go west. (46,81)| |Z|Alterac Mountains|

R Hillsbrad Foothills |N|Go south down the hills.|
K Kill yetis |N|In the yeti cave (48,33).  Need [Yeti Fur] x10 for "Bartolo's Yeti Fur Cloak".| |L|3720 10|

B Soothing Spices |L|3713| |N|From vendor in Southshore (49,55.1).|
B Fine Thread |L|2321|
T Encrypted Letter
A Letter to Stormpike
T Syndicate Assassins
T Foreboding Plans |N|Skip the follow-up.|
T Soothing Turtle Bisque
T Missing Crystals
T Costly Menace

F Menethil Harbor
T The Eye of Paleth
A Cleansing the Eye

F Ironforge
T Letter to Stormpike |Z|Ironforge| |N|Hall of Explorers (72,15)|
A Further Mysteries
A Reclaimers' Business in Desolace |N|From Roetten Stonehammer who walks around nearby.|

F Stormwind City |N|Take the Deep Run Tram if you don't have the flight point.|
T Reassignment |Z|Stormwind City| |N|In Stormwind Keep. (73.7,17.6)|
T Cleansing the Eye |Z|Stormwind City| |N|In Stormwind Cathedral. (39,27)|
B Bolt of Woolen Cloth |L|2997| |N|Buy from AH or a Tailor.|
B Hillman's Cloak |L|3719| |N|Buy from AH or a Leatherworker.|

H Southshore |N|Or fly back if on cooldown.|
T Bartolo's Yeti Fur Cloak
T Further Mysteries |N|Ignore next quest.|
]]
end)