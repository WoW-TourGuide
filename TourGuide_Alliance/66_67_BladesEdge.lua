
TourGuide:RegisterGuide("Blade's Edge Mountains (66-67)", "Netherstorm (67-69)", "Alliance", function()
return [[F Orebor Harborage
A No Time for Curiosity
A Killing the Crawlers |N|Up the stairs outside the cave|
C Killing the Crawlers |N|In the cave, of course|
R Sylvanaar |N|North out of the cave|
A The Den Mother |N|On a wanted poster|
T No Time for Curiosity |N|First building on the left|
A The Encroaching Wilderness |N|Outside the building near the stone and benches|
T Killing the Crawlers |N|Next building to the North|
A The Bloodmaul Ogres
A Malaise |N|North again|
A Into the Draenethyst Mine |N|In the 'long' building|
A Strange Brew
h Sylvanaar
N Grab flight point |N|North end of town|
C The Encroaching Wilderness |N|South of town|
C Malaise |N|West at Veil Lashh (35,73)|
T Malaise |N|Find a book up in the trees, bridge at (34,76)|
A Scratches
C Scratches |N|Use the feathers on the purple circle, return to town|
T The Encroaching Wilderness
A Marauding Wolves
T Scratches
C Into the Draenethyst Mine |N|South, down in the ravine (38,74).  Kill ogres and collect brew on your way|
C Strange Brew |N|Get 5 extra brews|
C The Bloodmaul Ogres
N Make sure you have 5 extra brews!
R Sylvanaar
T The Bloodmaul Ogres
A The Bladespire Ogres
T Strange Brew
A Getting the Bladespire Tanked
T Into the Draenethyst Mine
C Marauding Wolves |N|East over the bridge near the Horde outpost (41,65)|
C The Den Mother |N|Down in the cave at (52,74)|
R Toshley's Station |N|(60,69)|
A Crystal Clear
A Picking Up Some Power Converters
N Grab flight point |N|To the East|
A Test Flight: The Zephyrium Capacitorium
C Test Flight: The Zephyrium Capacitorium |N|Talk to Rally, your flight will zap you and then throw you far away, down the hill near R-3D0, you won't get hurt|
T Test Flight: The Zephyrium Capacitorium
A Test Flight: The Singing Ridge
R Bladespire Outpost |N|To the North (57,60)|
C Picking Up Some Power Converters |N|Click the power converters, use the magneto collector on the electromental that comes out, then kill it|
R Toshley's Station
T Picking Up Some Power Converters
A Ride the Lightning
A Ridgespine Menace
A Cutting Your Teeth
A What Came First, the Drake or the Egg? |N|In the inn|
C Test Flight: The Singing Ridge |N|Sign the waiver in your bag, talk to Rally again|
C Ride the Lightning |N|North of where you land, pop on the magneto sphere, gather the glands|
C Ridgespine Menace |N|Stealth spiders near the spikes|
C Cutting Your Teeth |N|Most of the daggermaws are North of Death�s Door.  If you don�t have that uncovered, it�s the canyon you just got thrown over|
R Singing Ridge |N|Or run to Toshley's and get tossed over|
C What Came First, the Drake or the Egg? |N|Look for spiky looking eggs, use them, use the phase modulator on them, kill whatever the tiny dragon turns into|
C Crystal Clear |N|On your way back to Toshley's|
R Toshley's Station
T Crystal Clear
T Ride the Lightning
A Trapping the Light Fantastic
A Gauging the Resonant Frequency
T Test Flight: The Singing Ridge
T Ridgespine Menace
A More than a Pound of Flesh
T Cutting Your Teeth
T What Came First, the Drake or the Egg?
A Test Flight: Razaan's Landing
N Don't turn in the Test Flight quest!
R Singing Ridge
C Gauging the Resonant Frequency |N|Clear an area and lay down the scanners in a star pattern.  Stand in the center of the star|
T Gauging the Resonant Frequency
R Razaan's Landing |N|Rally can send you here|
C Trapping the Light Fantastic |N|Lay traps to catch the pink orbs.  If you lay the traps near the pink electric circles on the poles, you can keep grabbing the orbs in a row|
C More than a Pound of Flesh
T Trapping the Light Fantastic
A Show Them Gnome Mercy!
T More than a Pound of Flesh
T Test Flight: Razaan's Landing
R Razaan's Landing |N|Rally can send you here, again|
C Show Them Gnome Mercy! |N|Kill about 4 of the Razaani by the portal in the center of the village (66,44) and Nexus-Prince Razaan will appear. Kill him and loot the quest item|
R Toshley's Station
T Show Them Gnome Mercy!
F Sylvanaar
T The Den Mother
T Marauding Wolves
A Protecting Our Own
C Protecting Our Own |N|South of town, look for grove seedlings on the ground and click them|
T Protecting Our Own
A A Dire Situation
C A Dire Situation |N|South in the bloodmaul ravine, use the powder on the bloodmaul wolves|
R Bladespire Hold |N|Follow the road North|
A The Trappings of a Vindicator |N|Vuuleen in a cage (43,51)|
C Getting the Bladespire Tanked |N|Kill them after they get drunk|
C The Trappings of a Vindicator |N|Droggam (39,53), Mugdorg (42,57).  Pull the non-elite guards first, then drop a brew to pull the boss without the elite guards.  Drop the brew in the doorway, not to far away|
T The Trappings of a Vindicator
A Gorr'Dim, Your Time Has Come...
C Gorr'Dim, Your Time Has Come... |N|Gorr'Dim (40,49).  Same pull strategy as the last two bosses|
C The Bladespire Ogres
T Gorr'Dim, Your Time Has Come... |N|Skip "Planting the Banner"|
H Sylvanaar
T Getting the Bladespire Tanked
T The Bladespire Ogres
T A Dire Situation
F Toshley's Station
A Test Flight: Ruuan Weald
C Test Flight: Ruuan Weald |N|Make sure to use the weather vane in mid-air!|
f Grab flight point
A A Time for Negotiation... |N|Tree Warden at the center moonwell|
A Creating the Pendant
T Test Flight: Ruuan Weald |N|South edge of town|
A Culling the Wild |N|North edge of town|
A A Date with Dorgok |N|West edge of town|
A Crush the Bloodmaul Camp!
A Little Embers |N|West edge of town|
A From the Ashes
N You should be ~75% to 67 now
C Creating the Pendant |N|Northeast in Veil Ruuan, kill the arakkoa for 6 claws, go to the green summoning circle (64,33), use the claws, kill the raven|
C A Time for Negotiation... |N|Keep an eye out for Overseer Nuaar, a neutral Draenei.  He walks around the camps here.|
C Culling the Wild |N|Go south of town then east up the path. Work your way north as you kill the quest mobs|
C From the Ashes |N|Soil at (71,22), (71,20), (71,18), this can be a tough area if you have good fire resist|
C Little Embers
A Damaged Mask |N|If you don't have the item in your bag, kill Fel Corrupters until you get it.|

R Evergrove
T Damaged Mask
A Mystery Mask
T Mystery Mask
A Felsworn Gas Mask
T A Time for Negotiation... |N|Tree Warden at the center moonwell|
A ...and a Time for Action
A Poaching from Poachers
T Creating the Pendant
A Whispers of the Raven God
T Culling the Wild
T Little Embers
T From the Ashes
N You're probably honored with the Cenarion Expedition by now

R Forge Camp: Anger |N|Back up the hill (73,41)|
T Felsworn Gas Mask |N|Put on the gas mask you idjut! Talk to the communicator (73,40)|
A Deceive thy Enemy
C Deceive thy Enemy
T Deceive thy Enemy
N Skip "You're Fired!" unless there are people around to help you
N You should now be 67 or very close to it.

C Poaching from Poachers |N|Back down, Northwest of Evergrove|
C ...and a Time for Action
A Did You Get The Note? |N|Keep killing these mobs until you get the [Meeting Note]|
R Bloodmaul Camp |N|Follow the road north (57,27)|
C Crush the Bloodmaul Camp!
C A Date with Dorgok |N|Top of the tower (55,24).  He should drop [Gorgrom's Favor] also|

R Evergrove |N|Or hearth to Sylvanaar and fly to Evergrove|
T A Date with Dorgok
A Favor of the Gronn |N|Started by [Gorgrom's Favor] which dropped from Dorgok|
T Favor of the Gronn
A Pay the Baron a Visit
T Crush the Bloodmaul Camp!
T ...and a Time for Action
T Did You Get The Note?
A Wyrmskull Watcher
T Poaching from Poachers
A Whelps of the Wyrmcult
N You damn well better be 67 by now |N|If not 10% into it!|

R Circle of Blood |N|Follow path Southwest from town, down and to the West (53,41)|
T Pay the Baron a Visit
A Into the Churning Gulch
C Into the Churning Gulch |N|To the Southwest (48,43)|
T Into the Churning Gulch
A Goodnight, Gronn
C Goodnight, Gronn |N|Head back East to Grulloc (59,47).  Use the sleeping powder on him and grab the sack. He wakes up when you take the bag so use the powder again. Be careful, 2-3 hits and you're dead|
T Goodnight, Gronn
A It's a Trap!

R Evergrove
T It's a Trap!
A Gorgrom the Dragon-Eater
A Slaughter at Boulder'mok

T Wyrmskull Watcher |N|West over the Wyrmskull Bridge|
A Longtail is the Lynchpin
C Longtail is the Lynchpin |N|Through the cave, near the end|
T Longtail is the Lynchpin
A Meeting at the Blackwing Coven

C Whispers of the Raven God |N|Exit the cave, grind on arakkoa until you get their buff, then visit each totem in order (40,17), (42,22), (41,19), (40,23)|
A The Truth Unorbed |U|31489| |N|Grind on the Arakkoa till you get the item that starts this.|
C Slaughter at Boulder'mok
C Gorgrom the Dragon-Eater |N|Collect 3 totems, go to the alter (30,22), use the trap, place the totems after the elite dies in the trap|

R Blackwing Coven |N|To the South (31,32)|
N Kill Wyrmcults and collect 5 [Costume Scraps] |N|For "Meeting at the Blackwing Coven".  This place sucks, it�s pretty tough.  Beware when they�re low on life, they may try to cast some type of black blessing which turns them into a dragonkin with about 50% life and they start fireballing and doing fire nova.|
C Whelps of the Wyrmcult |N|Go in the cave and use the blackwhelp net on the dragon whelps in here.  You can get right next to eggs and they will hatch one for you to capture|
C Meeting at the Blackwing Coven |N|Head to the back of the cave and put on the costume|

H Sylvanaar
F Evergrove
T Whelps of the Wyrmcult
T Meeting at the Blackwing Coven
A Maxnar Must Die!
T The Truth Unorbed
A Treebole Must Know
T Whispers of the Raven God
T Gorgrom the Dragon-Eater
A Baron Sablemane Has Requested Your Presence
T Slaughter at Boulder'mok
N You should be roughly 30% through 67

R Raven's Wood |N|Run back across wyrmskull bridge and through the tunnel.  As you exit the tunnel go left up the path (37,22)|
T Treebole Must Know
A Exorcising the Trees
C Exorcising the Trees |N|Kill Dire Ravens for 5 feathers, also collect 5 orbs off the ground. If you're grouped with someone only one person needs to collect these.  Combine the items, get in range of a Leafbeard and use one.  Kill the Koi-Koi Spirit, don't kill the leafbeard|
T Exorcising the Trees

R Blackwing Coven |N|Southwest (32,33)|
C Maxnar Must Die! |N|In the same cave as before, deeper in (33,36)|

H Sylvanaar |N|Die and spirit rez at Sylvanaar if your stone isn't up|
F Evergrove
T Maxnar Must Die!

R Circle of Blood |N|(53,41)|
T Baron Sablemane Has Requested Your Presence |N|Skip "Massacre at Gruul's Lair" unless you get some friends to help.|]]
end)

