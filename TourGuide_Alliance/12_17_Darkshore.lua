TourGuide:RegisterGuide("Darkshore (12-17)", "Loch Modan (17-18)", "Alliance", function()
return [[

A Washed Ashore (Part 1)
T Flight to Auberdine
A Return to Nessa 
h Auberdine
F Rut'theran Village
T Return to Nessa
F Auberdine
A For Love Eternal |N|On the docks, west of Inn|
A Buzzbox 827 |N|Inn, upstairs|
A Cave Mushrooms
A The Red Crystal
A Bashal'Aran (Part 1) |N|Big house at north|
A Tools of the Highborne
A Plagued Lands |N|Southeast over the bridge|
A How Big a Threat? (Part 1) |N|Inside|
C Washed Ashore (Part 1) |N|On the beach, south of Auberdine|
C Buzzbox 827
T Buzzbox 827 |N|East of flightpath, right next to it|
A Buzzbox 411
T Washed Ashore (Part 1)
A Washed Ashore (Part 2)
C Washed Ashore (Part 2) |N|Southwest of dock, northeast of the small island. Underwater|
C Buzzbox 411 |N|Threshers are in the sea|
T Washed Ashore (Part 2)
T Buzzbox 411 |N|Follow the road north out of Auberdine. When you reach Bashal'Aran, head northwest above the cove. Buzzbox is on the beach north of the cove (41.96,28.62)|
A Buzzbox 323

A Beached Sea Creature (Part 1) |N|South of the Buzzbox 411 at the cove|
N Beached Sea Creatures / Beached Sea Turtles |N|Notice! You will see many Beached Sea Creatures and Beached Sea Turtles on the beach while doing Darkshore! Gather them all if possible!|

R Bashal'Aran |N|Grind your way to Bashal'Aran|
T Bashal'Aran (Part 1)
A Bashal'Aran (Part 2)
C Bashal'Aran (Part 2)
T Bashal'Aran (Part 2)
A Bashal'Aran (Part 3)
C Bashal'Aran (Part 3)
T Bashal'Aran (Part 3)
A Bashal'Aran (Part 4)
C The Red Crystal |N|Grind your way to the mountains east of Auberdine|
C How Big a Threat? (Part 1) |N|Grind your way to northwest from Ameth'Aran to the Blackwood camp (47.32,48.67)|
R Twilight Vale |N|Follow the road south|
A The Fall of Ameth'Aran
C Tools of the Highborne
C For Love Eternal
C The Fall of Ameth'Aran |N|Look for the big white tablets on the ground (43.31,58.70) and (42.65,63.13)|
C Bashal'Aran (Part 4)
C Plagued Lands |N|West from Ameth'Aran|
T The Fall of Ameth'Aran
H Auberdine
T Plagued Lands |N|Next to the house east in Auberdine|
A Cleansing of the Infected
T How Big a Threat? (Part 1)
A How Big a Threat? (Part 2)
A Thundris Windweaver |N|Same NPC|
A The Tower of Althalaxx (Part 1) |N|Upstairs|
T Tools of the Highborne |N|The big house northwest|
T Thundris Windweaver |N|Same NPC|
A The Cliffspring River
T The Red Crystal
A As Water Cascades
N Fill the [Empty Water Tube] in the Moonwell |U|14338| |L|14339|
T For Love Eternal |N|West of Inn, at the dock|

T Beached Sea Creature (Part 1)

R Bashal'Aran |N|Follow the road north out of Auberdine|
T Bashal'Aran (Part 4)
C Cleansing of the Infected |N|You might want to head north while doing this quest|
C Buzzbox 323 |N|You might want to head north while doing this quest|

R Cliffspring River |N|Waterfall in east|
C The Cliffspring River |N|Go under the bridge into the water to get the sample| |U|12350|
T Buzzbox 323 |N|Buzzbox is on the other side of the bridge, left side|
A Buzzbox 525
R Cliffspring Falls |N|Follow the river to southeast|
C Cave Mushrooms |N|In the cave above waterfall|
T As Water Cascades |N|Follow the mountainline southwest until you are at the red crystal|
A The Fragments Within
C How Big a Threat? (Part 2) |N|Grind your way west across the road to Blackwood camp|

R Grove of the Ancients
T Grove of the Ancients
C Buzzbox 525 |N|Head southwest to the road, bears are at the other side of the road|
T Buzzbox 525 |N|Go back to the road, Buzzbox is next to two wooden fences and a fallen tree that has small path under it leading to the digsite|

H Auberdine

A Fruit of the Sea |N|West from flightpath|
T Cave Mushrooms
A Onu
T The Fragments Within
A The Absent Minded Prospector |N|North over the bridge|
T The Cliffspring River
A The Blackwood Corrupted
A WANTED: Murkdeep! |N|The Wanted poster infront of Inn|
T Cleansing of the Infected
A Tharnariun's Hope
T How Big a Threat? (Part 2)
N Fill the [Empty Cleansing Bowl] in the Moonwell |U|12346| |L|12347|

C The Blackwood Corrupted |N|Blackwood camps are east of Bashal'Aran. Gather grain (50.65,34.99), nuts (51.83,33.56) and fruit (52.87,33.38). Use the [Filled Cleansing Bowl] near bonfire. Kill Xabraxxis when he spanws| |U|12347|
C Tharnariun's Hope |N|To east between the two camps, up the hill is a cave. Kill the adds, run away and reset the Den Mother. Kill her after|
T The Tower of Althalaxx (Part 1) |N|Go north, over the river until you are at the road to ruins. the NPC is next to two rocks|
A The Tower of Althalaxx (Part 2)
C The Tower of Althalaxx (Part 2) |N|Mobs around the tower, not inside|
T The Tower of Althalaxx (Part 2)
A The Tower of Althalaxx (Part 3)
C Fruit of the Sea
H Auberdine
T Fruit of the Sea
T The Blackwood Corrupted
T Tharnariun's Hope
b Menethil Harbor
f Grab flight point
R Algaz Station |N|In Loch Modan. Watch out the big bad highlevel mobs on the way. Good luck!|

]]

end)