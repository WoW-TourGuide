TourGuide:RegisterGuide("Azuremyst Isle (1-12)", nil, "Alliance", function()
return [[
A You Survived!
T You Survived!
A Replenishing the Healing Crystals
A Volatile Mutations	
C Volatile Mutations
C Replenishing the Healing Crystals
T Volatile Mutations
A What Must Be Done...
A Botanical Legwork
T Replenishing the Healing Crystals
A Urgent Delivery!
T Urgent Delivery!
A Rescue the Survivors!
t Train (level 2)
A Shaman Training |C|Shaman|
T Shaman Training |C|Shaman|
A Spare Parts
A Inoculation
C Rescue the Survivors! |N|Find a Draenei Survivor, use your spell Gift of Naaru on him|
C Botanical Legwork |N|South of Ammen Fields|
C What Must Be Done...
T Botanical Legwork
T What Must Be Done...
A Healing the Lake
T Rescue the Survivors!
C Healing the Lake |N|Southwest of Crash Site|
C Inoculation |N|Southeast of Crash Site|
C Spare Parts
H Ammen Vale
T Healing the Lake
t Train (level 4)
A Call of Earth (Part 1) |C|Shaman|
T Spare Parts
A Inoculation
A The Missing Scout
T Call of Earth (Part 1) |N|Northwest of Crash Site (71,40)| |C|Shaman|
A Call of Earth (Part 2) |C|Shaman|
C Call of Earth (Part 2) |C|Shaman|
T Call of Earth (Part 2) |C|Shaman|
A Call of Earth (Part 3) |C|Shaman|
T Call of Earth (Part 3) |N|At the Crash Site| |C|Shaman|
T The Missing Scout |N|Southwest of Crash Site (72,60)|
A The Blood Elves
C The Blood Elves
T The Blood Elves
A Blood Elf Spy
C Blood Elf Spy
A Blood Elf Plans |N|Use the item that drops from the Surveyor Candress|
R Crash Site
T Blood Elf Spy
T Blood Elf Plans
A The Emitter
T The Emitter
A Travel to Azure Watch
R ChangeMeZone |N|*** You need to change this to match the zone for next NPC (66,53)|
A Word from Azure Watch
A Red Snapper - Very Tasty!
C Red Snapper - Very Tasty!
T Red Snapper - Very Tasty!
A Find Acteon!
R Azure Watch
T Find Acteon!
A The Great Moongraze Hunt (Part 1)
A Medicinal Purpose
T Travel to Azure Watch
T Word from Azure Watch
h Azure Watch
t First Aid
t Train (Level 6)

R Odesyus' Landing |N|The beach south of Azure Watch|
A Cookie's Jumbo Gumbo
A A Small Start
C Cookie's Jumbo Gumbo
C A Small Start |N|Go east and Insert coords here for Map and Compass please :)|
R Odesyus' Landing
T A Small Start
A I've Got A Plant
T Cookie's Jumbo Gumbo
N Grind to level 7 if you aren't yet, you need it to accept next quests |LV|7|
A Reclaiming the Ruins
A Precious and Fragile Things Need Special Handling
C The Great Moongraze Hunt (Part 1)
C Medicinal Purpose
C I've Got A Plant |N|Hollowed out tree north of Odesyus' Landing (48,62), leafs around the ground|
R Azure Watch
T Medicinal Purpose
A An Alternative Alternative
T The Great Moongraze Hunt (Part 1)
A The Great Moongraze Hunt (Part 2)
C An Alternative Alternative
N Kill all Infected Nightstalker Runts for a Questitem |L|23678 1|
A Strante Findings |N|Item in your inventory|
R Wrathscale Point
C Reclaiming the Ruins
C Precious and Fragile Things Need Special Handling
N Kill naga's until they drop a Quest |L|23759 1|
A Rune Covered Tablet |N|Item in your inventory|
R Odesyus' Landing
T I've Got A Plant
A Tree's Company
T Rune Covered Tablet
A Warlord Sriss'tiz |N|After the NPC's stop talking|
T Reclaiming the Ruins
T Precious and Fragile Things Need Special Handling
R Azure Watch
A The Missing Fisherman
A Learning The Language
C Learning The Language |N|Read to book in your inventory|
T Learning The Language
A Totem of Coo |N|No need to follow the NPC, point will be shown later|
T Strange Findings
A Nightstalker Clean Up, Isle 2...
t Train (level 8)
C The Great Moongraze Hunt (Part 2) |N|North of Azure Watch|
T Totem of Coo |N|On top the the cliifs north of Azure Watch (55,41)|
A Totem of Tikti
T Totem of Tikti |N|Jump to northeast with your wings, run over water to (64,39)|
A Totem of Yor
T Totem of Yor |N|*** Enter description here (63,47)|
A Totem of Vark 
T Totem of Vark |N|Follow the NPC|
A The Prophecy of Akida
C The Prophecy of Akida
C Nightstalker Clean Up, Isle 2...
R Tides' Hollow
C Warlord Sriss'tiz |N|In the cave, at lower level|
R TheIslandInSouthwest |N|Go to the beach with a flag (18,83)|
N Use [Tree Disguide Kit] |U|23792|
C The Missing Fisherman |N|At the dock (28,82)|
A All That Remains
C All That Remains
T All That Remains
H Azure Watch
T An Alternative Alternative
T The Prophecy of Akida
A Stillpine Hold
T The Great Moongraze Hunt (Part 2)
R Odesyus' Landing
T Tree's Company
A Show Gnomercy
T Warlord Sriss'tiz
C Show Gnomercy |N|Talk to the gnome that wanders at the beach|
T Show Gnomercy
A Deliver Them From Evil...
R Azure Watch
T Nightstalker Clean Up, Isle 2...
T Deliver Them From Evil...
N Grind until you are level 10 |LV|10|
t Train (level 10)
A Coming of Age
A Call of Fire (Part 1) |C|Shaman|
R Exodar
T Coming of Age
A Elekks Are Serious Business
R Stillpine Hold
A Beasts of the Apocalypse!
A Murlocs... Why Here? Why Now?
T Stillpine Hold
C Beasts of the Apocalypse! |N|Northeast of Stillpine Hold|
T Call of Fire (Part 1) |N|Go further northeast| |C|Shaman|
A Call of Fire (Part 2) |C|Shaman|
R Stillpine Hold
T Beasts of the Apocalypse!
A Chieftain Oomooroo
A Search Stillpine Hold
N Kill mobs until you get the questitem for your Shamanquest |L|23733 1| |C|Shaman|
C Chieftain Oomooroo
T Search Stillpine Hold |N|There's a red crystal in the cave you need to touch|
A Blood Crystals |N|Spawns 2 owlkins|
T Blood Crystals
T Chieftain Oomooroo
A The Kurken is Lurkin
C The Kurken is Lurkin |Kill the 2 headed dog next to the red crystal, in the cave|
T The Kurken is Lurkin
A The Kurken's Hide
C The Kurken's Hide
T Call of Fire (Part 2) |C|Shaman|
A Call of Fure (Part 3) |C|Shaman|
C Murlocs... Why Here? Why Now? |N|*** Westshore. Describe it better|
N Kill the named Murloc called Murgurgula to get a quest |L|23850 1|
A Gurf's Dignity
R Stillpine Hold
T Murlocs... Why Here? Why Now?
T Gurf's Dignity
R Kessel's Crossing |N|The second island north of Azuremyth Isle|
A A Favorite Treat
T Elekks Are Serious Business
A Alien Predators
A The Kessel Run |N|Timed quest, 15 minutes. Deliver word to 3 places|
C The Kessel Run |N|Talk to High Chieftain Stillpine at Stillpine Hold (46,20), SomeoneSomeone at Azure Watch (47,50), SomeoneSomeone at Odesyus' Landing (46,70)|
R Kessel's Crossing
T The Kessel Run

A Declaration of Power |C|Shaman|
R Use the elekk and ride to the island southwest of Azuremyst Isle |N|*** Replace this with the name and description :D (11,82)| |C|Shaman|
C Call of Fire (Part 3) |N|Wickerman Effigy (11,82)| |C|Shaman|
U Orb of Returning |C|Shaman|
T Call of Fire (Part 3) |C|Shaman|
A Call of Fire (Part 4) |C|Shaman|
H Azure Watch
T Call of Fire (Part 4) |C|Shaman|
A Call of Fire (Part 5) |C|Shaman|
R Exodar
T Call of Fire (Part 5) |C|Shaman|

R Kessel's Crossing |C|Shaman|
N You need to select the next island (12-20 Bloodmyst Isle) manually now! |N|You need to continue questing in that island in order to get your Totem-quests done| |C|Shaman|
B Auberdine |C|Warrior|
B Auberdine |C|Paladin|
B Auberdine |C|Hunter|
B Auberdine |C|Priest|
B Auberdine |C|Mage|

]]
end)