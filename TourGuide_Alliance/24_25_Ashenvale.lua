TourGuide:RegisterGuide("Ashenvale (24-25)", "Wetlands (25-27)", "Alliance", function()
return [[
T Pridewings of Stonetalon
A Kayneth Stillwind
T Trek to Ashenvale
T The Ruins of Stardust
A Fallen Sky Lake
R Fire Scar Shrine |N|Southwest from Astranaar. Kill all mobs on the way|
C The Tower of Althalaxx (Part 5)
C Culling the Threat |N|North from Astranaar|
R Maestra's Post
T The Tower of Althalaxx (Part 5)
A The Tower of Althalaxx (Part 6)
A Supplies to Auberdine |N|Escort|
R Maestra's Post
T Supplies to Auberdine
R Astranaar
T Culling the Threat
R Silverwind Refuge
A Elemental Bracers
N Collect 5x [Intact Elemental Bracers] |N|From the elementals in the lake south of Silverwind Refuge |L|12220 5|
C Elemental Bracers |U|5456|
T Elemental Bracers
H Auberdine
b Menethil Harbor

]]
end)